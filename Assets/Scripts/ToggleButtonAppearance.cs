﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleButtonAppearance : MonoBehaviour {

    [Header ("True = Sound, False = Music")]
    public bool soundOrMusic;

    public Sprite sound;
    public Sprite music;
    public Sprite noSound;
    public Sprite noMusic;
    private bool previousState = false;

    

    // Use this for initialization
    void Start () {
		
	}

    private void OnEnable()
    {
        if (soundOrMusic)
        {
            if (SoundManager.soundMuted)
            {
                this.GetComponent<Image>().sprite = noSound;
            }
            else
            {
                this.GetComponent<Image>().sprite = sound;
            }
        }
        else
        {
            if (MusicManager.musicMuted)
            {
                this.GetComponent<Image>().sprite = noMusic;
            }
            else
            {
                this.GetComponent<Image>().sprite = music;
            }
        }

    }

    // Update is called once per frame
    void Update () {
        if (soundOrMusic)
        {
            if(SoundManager.soundMuted != previousState)
            {
		        OnEnable();
                previousState = SoundManager.soundMuted;
            }
        }
        else
        {
            if (MusicManager.musicMuted != previousState)
            {
                OnEnable();
                previousState = MusicManager.musicMuted;
            }
        }

    }
}
