﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GoToScene : MonoBehaviour
{

    public void LoadScene(string sceneName)
    {
        StartCoroutine(FadeInBlackscreen(sceneName, true, true));
    }

    public IEnumerator FadeInBlackscreen(string sceneName, bool ab, bool c)
    {
        if (ab)
        {
            GameObject blackScreen = Instantiate(Resources.Load("Blackboi", typeof(GameObject))) as GameObject;
            blackScreen.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);

            for (float i = 0; i < 1; i += 0.02f)
            {

                blackScreen.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, i);
                yield return new WaitForSeconds(0.01f);
            }

        }

        SceneManager.LoadScene(sceneName);
        EventManager.instance.RunEvent("SwitchScene_" + sceneName);

    }




}
