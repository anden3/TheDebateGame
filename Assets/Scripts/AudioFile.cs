﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class FilterSettings
{
    public bool overrideDefault = false;
}

[System.Serializable]
public class DistortionFilterSettings : FilterSettings
{
    [Space]

    [Tooltip("Distortion value.")]
    [Range(0, 1)] public float distortionLevel = 0.5f;
}

[System.Serializable]
public class EchoFilterSettings : FilterSettings
{
    [Space]

    [Tooltip("Echo delay in ms. Only seem to work for values <= ~500")]
    [Range(10, 5000)] public int delay = 500;

    [Tooltip("Echo decay per delay. 1.0 = No decay, 0.0 = total decay (i.e. simple 1 line delay).")]
    [Range(0, 1)] public float decayRatio = 0.5f;

    [Space]

    [Tooltip("Volume of echo signal to pass to output.")]
    [Range(0, 1)] public float wetMix = 1.0f;

    [Tooltip("Volume of original signal to pass to output.")]
    [Range(0, 1)] public float dryMix = 1.0f;
}

[System.Serializable]
public class HighPassFilterSettings : FilterSettings
{
    [Space]

    [Tooltip("Highpass cutoff frequency in Hertz.")]
    [Range(10, 22000)] public float cutoffFrequency = 5000;

    [Tooltip("Highpass resonance quality value, determines how much the filter’s self-resonance is dampened.")]
    [Range(1, 10)] public float highpassResonanceQ = 1.0f;
}

[System.Serializable]
public class LowPassFilterSettings : FilterSettings
{
    [Space]

    [Tooltip("Lowpass cutoff frequency in Hertz.")]
    [Range(10, 22000)] public float cutoffFrequency = 5000;

    [Tooltip("Lowpass resonance quality value, determines how much the filter’s self-resonance is dampened.")]
    [Range(1, 10)] public float lowpassResonanceQ = 1.0f;
}

[System.Serializable]
public class ReverbFilterSettings : FilterSettings
{
    [Space]

    [Tooltip("Mix level of dry signal in output in mB.")]
    [Range(-10000, 0)] public float dryLevel = 0;

    [Space]

    [Tooltip("Room effect level at low frequencies in mB.")]
    [Range(-10000, 0)] public float room = 0;

    [Tooltip("Room effect high-frequency level in mB.")]
    [Range(-10000, 0)] public float roomHF = 0;

    [Tooltip("Room effect low-frequency level in mB.")]
    [Range(-10000, 0)] public float roomLF = 0;

    [Space]

    [Tooltip("Reverberation decay time at low-frequencies in seconds.")]
    [Range(0.1f, 20)] public float decayTime = 1.0f;

    [Tooltip("High-frequency to low-frequency decay time ratio.")]
    [Range(0.1f, 2)] public float decayHFRatio = 0.5f;

    [Space]

    [Tooltip("Early reflections level relative to room effect in mB.")]
    [Range(-10000, 1000)] public float reflectionsLevel = -10000;

    [Tooltip("Early reflections delay time relative to first reflection in seconds.")]
    [Range(0, 0.3f)] public float reflectionsDelay = 0;

    [Space]

    [Tooltip("Late reverberation level relative to room effect in mB.")]
    [Range(-10000, 2000)] public float reverbLevel = 0;

    [Tooltip("Late reverberation delay time relative to first reflection in seconds.")]
    [Range(0, 0.1f)] public float reverbDelay = 0.04f;

    [Space]

    [Tooltip("Reference high frequency in Hz.")]
    [Range(20, 20000)] public float hfReference = 5000;

    [Tooltip("Reference low frequency in Hz.")]
    [Range(20, 1000)] public float lfReference = 250;

    [Space]

    [Tooltip("Reverberation diffusion (echo density) in percent.")]
    [Range(0, 100)] public float diffusion = 100;

    [Tooltip("Reverberation density (modal density) in percent.")]
    [Range(0, 100)] public float density = 100;
}

public class AudioFile : MonoBehaviour
{
    [Header("Component References")]
    public AudioClip file;
    public AudioMixerGroup mixer;

    [Header("General Settings")]
    [Tooltip("If sound can play without getting interrupted by other sound. Do not use this if sound plays during scene changes.")]
    public bool isOneShot = true;
    public bool loop = false;

    [Range(0, 1)] public float volume = 1;
    [Range(-3, 3)] public float pitch = 1;

    [Tooltip("Small priority means more likely to be played.")]
    [Range(0, 256)] public int priority = 128;

    [Header("Audio Filter Settings")]
    public DistortionFilterSettings distortionFilterSettings;
    public EchoFilterSettings echoFilterSettings;
    public HighPassFilterSettings highPassFilterSettings;
    public LowPassFilterSettings lowPassFilterSettings;
    public ReverbFilterSettings reverbFilterSettings;
}
