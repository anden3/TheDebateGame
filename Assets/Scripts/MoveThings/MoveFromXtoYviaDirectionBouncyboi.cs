﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFromXtoYviaDirectionBouncyboi : MonoBehaviour {
    [Header("Needs rigidbody without gravity plz")]
    
    public Vector2 startPos = new Vector2(0, 10);
    public Vector2 endPos = new Vector2(0, 0);

    public float startAcceleration = 5;
    public float maxAcceleration = 250;

    private float acceleration;

    private Vector2 normalizedDirectionOnStart;

    private bool directionFlipped = false;
    private bool checkEventBool;

    private bool isPlaying = false;

    private int directionFlippedCount = 0;
    
	void Start () {
        GetComponent<Rigidbody2D>().gravityScale = 0;
        transform.position = startPos;
        directionFlipped = false;

        acceleration = startAcceleration;

        checkEventBool = directionFlipped;

        normalizedDirectionOnStart = (endPos - startPos) / Vector2.Distance(startPos, endPos);
    }

    void FixedUpdate()
    {
        if (!isPlaying) return;

        Vector2 currentDirectionToEnd = (endPos - (Vector2)transform.position) / Vector2.Distance(transform.position, endPos);

        directionFlipped = (normalizedDirectionOnStart == currentDirectionToEnd) ? false : true;

        if (directionFlipped != checkEventBool)
        {
            if (Vector2.Distance(transform.position, endPos) > 0.05f && acceleration > startAcceleration)
            {
                transform.position = endPos;
                GetComponent<Rigidbody2D>().simulated = false;
                isPlaying = false;
            }

            if (acceleration < maxAcceleration)
            {
                switch(directionFlippedCount)
                {
                    case 0: acceleration = acceleration * 16; break;
                    case 1: acceleration = acceleration * 0.5f; break;
                    case 2: acceleration = acceleration * 1.2f; break;
                }
                directionFlippedCount++;
            }

            checkEventBool = directionFlipped;
        }

        //Debug.Log(directionFlipped);
        //Debug.Log(acceleration);
        //Debug.Log(normalizedDirectionOnStart);
        //Debug.Log(currentDirectionToEnd);

        GetComponent<Rigidbody2D>().AddForce(currentDirectionToEnd * acceleration);
    }

    public void StartMovement()
    {
        //sh
        isPlaying = true;
        Start();
    }
}
