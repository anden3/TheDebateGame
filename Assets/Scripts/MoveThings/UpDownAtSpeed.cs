﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDownAtSpeed : MonoBehaviour {
    public float timeBetweenSwaps = 2f;
    public float force = 0.1f;
    public float distanceWobble = 0.2f;

    private bool direction = true;

    private Vector2 origin;

    private Rigidbody2D rb;

    private float originSpeed;

    private bool lastThing;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        origin = transform.position;
        lastThing = direction;
        originSpeed = force;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (direction)
        {
            rb.AddForce(Vector2.down * force);
            Debug.Log(transform.position.y + " " + (origin.y - distanceWobble));
            if (transform.position.y > origin.y - distanceWobble) direction = true;
            else direction = false;
        }
        else
        {
            rb.AddForce(Vector2.up * force);
            Debug.Log(transform.position.y + " " + (origin.y + distanceWobble));
            if (transform.position.y < origin.y + distanceWobble) direction = false;
            else direction = true;
        }

        if(Vector2.Distance(transform.position, origin) > distanceWobble)
        force = (Vector2.Distance(transform.position, origin) * 2) * originSpeed;
	}
        
}
