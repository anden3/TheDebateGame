﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBetweenNPointsBouncyboi : MonoBehaviour {
    [Header("Needs rigidbody without gravity plz")]

    public bool resetOnEnd = true;
    public bool playOnLoad = false;
    public bool doSoundOnBounce = false;

    public Vector2[] points;

    public float startAcceleration = 5;
    public float maxAcceleration = 250;
    public float waitOnEachPoint = 0.2f;

    private float acceleration;

    private Vector2 normalizedDirectionOnStart;
    private int currentPoint = 0;

    private bool directionFlipped = false;
    private bool checkEventBool;

    private bool isPlaying = false;

    private int directionFlippedCount = 0;

    void Start()
    {
        Debug.Assert(GetComponent<Rigidbody2D>());
        Debug.Assert(points.Length > 0);

        GetComponent<Rigidbody2D>().gravityScale = 0;
        GetComponent<Rigidbody2D>().simulated = true;
        currentPoint = 0;
        directionFlipped = false;
        directionFlippedCount = 0;

        transform.position = points[currentPoint];

        acceleration = startAcceleration;

        checkEventBool = directionFlipped;

        normalizedDirectionOnStart = (points[currentPoint + 1] - points[currentPoint]) / Vector2.Distance(points[currentPoint], points[currentPoint + 1]);

        if (playOnLoad) isPlaying = true;
    }

    void FixedUpdate()
    {
        if (!isPlaying) return;
        //Debug.Log("START");
        //Debug.Log(isPlaying);
        //Debug.Log(currentPoint);
        //Debug.Log(directionFlippedCount);
        //Debug.Log(directionFlipped);
        //Debug.Log("END");

        Vector2 currentDirectionToEnd = (points[currentPoint + 1] - (Vector2)transform.position) / Vector2.Distance(transform.position, points[currentPoint + 1]);

        directionFlipped = (normalizedDirectionOnStart == currentDirectionToEnd) ? false : true;

        if (directionFlipped != checkEventBool)
        {
            if (directionFlippedCount >= 2)
            {
                if(doSoundOnBounce && EventManager.instance)EventManager.instance.RunEvent("BouncyboiDoABounce");
                transform.position = points[currentPoint + 1];
                GetComponent<Rigidbody2D>().simulated = false;
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                isPlaying = false;
                StartCoroutine(EndOfCurrentPoint());
                return;
            }

            switch (directionFlippedCount)
            {
                case 0: acceleration = acceleration * 15; break;
                case 1: acceleration = acceleration * 0.5f; break;
                case 2: acceleration = acceleration * 1.1f; break;
            }
            if(directionFlippedCount <= 3) directionFlippedCount++;

            checkEventBool = directionFlipped;
        }

        GetComponent<Rigidbody2D>().AddForce(currentDirectionToEnd * acceleration);
    }
    
    IEnumerator EndOfCurrentPoint()
    {
        if (currentPoint + 2 < points.Length)
        {
            yield return new WaitForSeconds(waitOnEachPoint);

            currentPoint++;

            transform.position = points[currentPoint];
            directionFlipped = false;
            directionFlippedCount = 0;

            acceleration = startAcceleration;

            checkEventBool = directionFlipped;

            normalizedDirectionOnStart = (points[currentPoint + 1] - points[currentPoint]) / Vector2.Distance(points[currentPoint], points[currentPoint + 1]);

            GetComponent<Rigidbody2D>().simulated = true;

            isPlaying = true;
        }
        else
        {
            if(resetOnEnd)
            {
                GetComponent<Rigidbody2D>().simulated = false;
                transform.position = points[0];
                isPlaying = false;
            }
            else
            {
                GetComponent<Rigidbody2D>().simulated = false;
                isPlaying = false;
            }
        }
    }

    public void StartMovement()
    {
        //sh
        Start();
        isPlaying = true;
    }
}
