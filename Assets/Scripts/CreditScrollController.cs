﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditScrollController : MonoBehaviour
{
    public float scrollSpeed;
    public int endPosition;

    private RectTransform rt;

    void Awake()
    {
        rt = GetComponent<RectTransform>();
    }
	
    void FixedUpdate()
    {
        rt.position = new Vector2(0, transform.position.y + scrollSpeed);

        if (rt.position.y >= endPosition)
        {
            GameManager.instance.ExitGame();
        }
    }
}
