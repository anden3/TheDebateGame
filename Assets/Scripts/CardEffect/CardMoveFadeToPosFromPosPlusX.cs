﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardMoveFadeToPosFromPosPlusX : MonoBehaviour {
    
    public float scale = 0.3f;
    public float startSpeed = 5f;
    public float endSpeed = 1f;
    public float decreaseSpeedPerFrame = 0.1f;
    [Space]
    public float startOpacity = 0f;
    public float endOpacity = 1f;
    public float increaseOpacityPerFrame = 0.05f;
    [Space]
    public float minimumDistanceToMove = 4f;
    public bool distanceAffectsTravel = false;

    [System.NonSerialized]
    public Vector3 startPos;
    [System.NonSerialized]
    public Vector3 endPos;

    private Vector3 newStartPos;

    private float speed;
    private float opacity;

    void Start()
    {
        transform.localScale = new Vector2(scale, scale);
        speed = startSpeed;
        opacity = startOpacity;
        this.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, opacity);

        float dist = Vector3.Distance(startPos, endPos);
        if(distanceAffectsTravel)
        {
            float newDist = (Mathf.Clamp(dist, 0, 10) / 10);
            minimumDistanceToMove = minimumDistanceToMove * newDist;
        }
        float deltaX = startPos.x - endPos.x;
        float deltaY = startPos.y - endPos.y;
        Vector3 newVector = new Vector3((minimumDistanceToMove / dist) * deltaX, (minimumDistanceToMove / dist) * deltaY, 0);
        newStartPos = newVector + endPos;

        transform.position = newStartPos;
    }

    void FixedUpdate()
    {
        decreaseSpeedPerFrame = startSpeed / (startSpeed / Vector3.Distance(transform.position, endPos)) * Time.fixedDeltaTime;

        float step = speed * Time.fixedDeltaTime;
        transform.position = Vector3.MoveTowards(transform.position, endPos, step);

        if (speed > endSpeed) speed -= decreaseSpeedPerFrame;

        if (opacity < endOpacity) opacity += (increaseOpacityPerFrame);

        if (Vector3.Distance(transform.position, endPos) < 0.02f)
        {
            EventManager.instance.RunEvent("DestroyCard_FadeAwayPosPosPlusX");
            Destroy(gameObject);
        }

        this.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, opacity);
    }
}
