﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardMoveFadeAwayFromPosToPos : MonoBehaviour {

    public float scale = 0.3f;
    public float startSpeed = 2f;
    public float endSpeed = 20f;
    public float increaseSpeedPerFrame = 1f;
    [Space]
    public float startOpacity = 1f;
    public float endOpacity = 0f;
    public float decreaseOpacityPerFrame = 0.05f;

    [System.NonSerialized]
    public Vector3 startPos;
    [System.NonSerialized]
    public Vector3 endPos;

    private float speed;
    private float opacity;

    void Start()
    {
        transform.localScale = new Vector2(scale, scale);
        speed = startSpeed;
        opacity = startOpacity;
        transform.position = startPos;
    }

    void FixedUpdate()
    {
        float step = speed * Time.fixedDeltaTime;
        transform.position = Vector3.MoveTowards(transform.position, endPos, step);

        if (speed < endSpeed) speed += (increaseSpeedPerFrame * 5);
        if (opacity > endOpacity) opacity -= (decreaseOpacityPerFrame / 1.5f);

        if (opacity <= endOpacity)
        {
            EventManager.instance.RunEvent("DestroyCard_FadeAwayPosPos");
            Destroy(gameObject);
        }

        this.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, opacity);
    }
}
