﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnLoadSendEvent : MonoBehaviour
{
    public string sceneName;
    private GameObject blackScreen;

    private void Start()
    {
        EventManager.instance.RunEvent(sceneName + "HasLoaded");
        StartCoroutine(FadeOutBlackscreen());
    }
    public IEnumerator FadeOutBlackscreen()
    {
        blackScreen = Instantiate(Resources.Load("Blackboi", typeof(GameObject))) as GameObject;

        for (float i = 1; i > 0; i -= 0.025f)
        {

            blackScreen.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, i);
            yield return new WaitForSeconds(0.01f);
            
        }

        Destroy(blackScreen);
        //Debug.Log(GameManager.instance.isActiveAndEnabled);
    }

}
