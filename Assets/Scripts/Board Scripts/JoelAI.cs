﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading;

public class JoelAI : MonoBehaviour {

    public class CardCombination
    {
        public int supportChange;
        public int trustChange;
        public int trustChangePlayer;
        public float timeChange;
        public GameObject Card1;
        public GameObject Card2;
    }
    public enum Mode
    {
        HighestTrustSupport,
        SnipePlayerTrust
    }

    public bool printAIList = false;
    [Space]
    public GameObject enemyDeckHand;
    public GameObject enemyPlayedCards;
    public GameObject playerPlayedCards;

    [Space]
    public float selectTopX = 1;
    public float supportMultiplier = 1;
    public float trustMultiplier = 1;
    [Space]
    public Mode aiMode = Mode.HighestTrustSupport;
    public int snipeAtTrust = 5;

    [Space]
    [Header("No touch plz")]
    public List<bool> requirementsMet = new List<bool>();
    public List<CardContainer> chosenCards = new List<CardContainer>();
    public List<CardCombination> allPossibleCombinations = new List<CardCombination>();

    public static JoelAI instance = null;

    private BoardManager boardManager;

    private CardCombination winner = null;
    private CardContainer con1 = null;
    private CardContainer con2 = null;

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        boardManager = BoardManager.instance;
        selectTopX = Mathf.Clamp(selectTopX, 1, 20);
    }

    public void ChooseCards()
    {
        //clear
        allPossibleCombinations.Clear();
        chosenCards.Clear();
        winner = null;
        con1 = null;
        con2 = null;

        //do the thing
        MakeAllCombiantions();

        //now do whatever you want with the combinations

        OrderListSelectWinner();



        //Debug.Log(allPossibleCombinations.Count);
        //foreach (CardCombination a in allPossibleCombinations)
        //{
        //    Debug.Log("Card1: " + a.Card1.transform.GetChild(0).name + " Card2: " + a.Card2.transform.GetChild(0).name
        //+ " With combined T/S: " + a.trustChangePlayer);
        //}
        //foreach (CardCombination a in allPossibleCombinations)
        //{
        //    Debug.Log("Card1: " + a.Card1.transform.GetChild(0).name + " Card2: " + a.Card2.transform.GetChild(0).name
        //+ " With combined T/S: " + ((a.supportChange * supportMultiplier) + (a.trustChange * trustMultiplier)));
        //}

        Debug.Assert(winner != null);
        Debug.Assert(con1 != null);
        Debug.Assert(con2 != null);
        AddChoisesToList(con1, con2);
    }

    void OrderListSelectWinner()
    {
        int tempTop = (int)Random.Range(1, selectTopX);
        //OKEY SHHHHHHHH, sh. jättefint. 
        if (aiMode == Mode.SnipePlayerTrust)
        {
            if (boardManager.trustPlayer >= snipeAtTrust)
            {
                allPossibleCombinations = allPossibleCombinations.OrderByDescending(
                c => c.trustChangePlayer
                ).ToList();

                //only take top bc anti trust move
                winner = allPossibleCombinations[allPossibleCombinations.Count - 1];
                con1 = winner.Card1.GetComponent<CardContainer>();
                con2 = winner.Card2.GetComponent<CardContainer>();
                PrintOutAllCombinations(Mode.SnipePlayerTrust);
                //if no change, then do other
                if (allPossibleCombinations[allPossibleCombinations.Count - 1].trustChangePlayer == 0)
                {
                    allPossibleCombinations = allPossibleCombinations.OrderBy(
                    c => ((c.supportChange * supportMultiplier) + (c.trustChange * trustMultiplier))
                    ).ToList();

                    winner = allPossibleCombinations[allPossibleCombinations.Count - tempTop];
                    con1 = winner.Card1.GetComponent<CardContainer>();
                    con2 = winner.Card2.GetComponent<CardContainer>();
                    PrintOutAllCombinations(Mode.HighestTrustSupport);
                }
            }
            else
            {
                allPossibleCombinations = allPossibleCombinations.OrderBy(
                c => ((c.supportChange * supportMultiplier) + (c.trustChange * trustMultiplier))
                ).ToList();

                winner = allPossibleCombinations[allPossibleCombinations.Count - tempTop];
                con1 = winner.Card1.GetComponent<CardContainer>();
                con2 = winner.Card2.GetComponent<CardContainer>();
                PrintOutAllCombinations(Mode.HighestTrustSupport);
            }
        }
        else if (aiMode == Mode.HighestTrustSupport)
        {
            allPossibleCombinations = allPossibleCombinations.OrderBy(
                c => ((c.supportChange * supportMultiplier) + (c.trustChange * trustMultiplier))
            ).ToList();

            winner = allPossibleCombinations[allPossibleCombinations.Count - tempTop];
            con1 = winner.Card1.GetComponent<CardContainer>();
            con2 = winner.Card2.GetComponent<CardContainer>();
            PrintOutAllCombinations(Mode.HighestTrustSupport);
        }
    }

    void PrintOutAllCombinations(Mode mode)
    {
        if (!printAIList) return;
        int counter = 0;
        Debug.Log("--------------START-------------");
        switch(mode)
        {
            case Mode.SnipePlayerTrust:
                foreach(CardCombination a in allPossibleCombinations)
                {
                    Debug.Log("Nr" + counter + ": " +"Card1: " + a.Card1.transform.GetChild(0).name + " Card2: "+ a.Card2.transform.GetChild(0).name + " ChangedPlayerTrustBy: "+a.trustChangePlayer);
                    counter++;
                }
                break;
            case Mode.HighestTrustSupport:
                foreach (CardCombination a in allPossibleCombinations)
                {
                    Debug.Log("Nr" + counter + ": " + "Card1: " + a.Card1.transform.GetChild(0).name + " Card2: " + a.Card2.transform.GetChild(0).name + " Combined TrustandSupportValue: " + ((a.trustChange*trustMultiplier)+(a.supportChange*supportMultiplier)));
                    counter++;
                }
                break;
        }
        Debug.Log("--------------END-------------");
    }

    void AddChoisesToList(CardContainer con1, CardContainer con2)
    {
        //find card with same name in playing field then add that.
        

        chosenCards.Add(FindCardInHand(con1.card));
        chosenCards.Add(FindCardInHand(con2.card));
    }

    CardContainer FindCardInHand(BaseCardClass cardToFind)
    {
        //if containers card has same name as input card, thats the right container
        foreach(Transform container in enemyDeckHand.transform)
        {
            if (container.GetComponent<CardContainer>().card.name == cardToFind.name)
            {
                return (container.GetComponent<CardContainer>());
            }
        }
        return null;
    }

    public void MakeAllCombiantions()
    {
        //they aren't containers but gameobjects, sh
        //for every child in enemyDeckHand
        foreach(Transform container in enemyDeckHand.transform)
        {
            //go through every child in enemyDeckHand and add to list of combinations
            foreach(Transform comparedWithContainer in enemyDeckHand.transform)
            {
                //if with same A, A continue, WORKS
                if (container.gameObject.GetInstanceID() == comparedWithContainer.gameObject.GetInstanceID())
                {
                    continue;
                }

                bool combinationAlreadyExists = false;
                //if same combination before, contine
                foreach(CardCombination combination in allPossibleCombinations)
                {
                    //BASICLY, these fuckers got (clone) at the back of their string names which makes my peepee very angEry
                    //Grrrrrr
                    string Card1Name = RemovePCloneP(combination.Card1.gameObject.name);
                    string Card2Name = RemovePCloneP(combination.Card2.gameObject.name);

                    //Debug.Log(".......");
                    //Debug.Log(container.transform.gameObject.name);
                    //Debug.Log(comparedWithContainer.transform.gameObject.name);
                    //Debug.Log(Card1Name);
                    //Debug.Log(Card2Name);
                    //Debug.Log("-------");

                    //since which side doesen't matter, both containers cant be in a single combinations more than once
                    //first if Container == any of cards in combination
                    if (container.gameObject.name == Card1Name
                        || container.gameObject.name == Card2Name)
                    {
                        //second if first container has either card, s
                        if (comparedWithContainer.gameObject.name == Card1Name
                        || comparedWithContainer.gameObject.name == Card2Name)
                        {
                            combinationAlreadyExists = true;
                            break;
                        }
                    }
                }
                if (combinationAlreadyExists) continue;

                //now checks are done, do what wanted
                //this part is a bit dodgey
                GameObject thisSetOfPlayedCards = new GameObject();
                GameObject copyContainer = Instantiate(container.gameObject);
                GameObject copyComparedWithContainer = Instantiate(comparedWithContainer.gameObject);

                copyContainer.transform.SetParent(thisSetOfPlayedCards.transform);
                copyComparedWithContainer.transform.SetParent(thisSetOfPlayedCards.transform);

                allPossibleCombinations.Add(CalculateCardValues(thisSetOfPlayedCards));

                //clear the new played
                Destroy(thisSetOfPlayedCards);
            }
        }
    }

    string RemovePCloneP(string inputString)
    {
        string newString = "";
        string thingToRemoveFromBack = "(Clone)";

        for(int i = 0; i < inputString.Length - thingToRemoveFromBack.Length; i++)
        {
            newString += inputString[i];
        }

        return newString;
    }

    CardCombination CalculateCardValues(GameObject currentPlayedCards)
    {
        GameObject opponentPlayedCards = playerPlayedCards;
        // Temporary values for support and trust.
        int trustChange = 0;
        int supportChange = 0;
        int trustChangePlayer = 0;
        float timeChange = 0;

        int currentChildIndex = 0;
        bool[] requirementsLocalArray = new bool[7];
        requirementsMet.Clear();


        foreach (Transform child in currentPlayedCards.transform)
        {
            if (child.childCount == 0)
            {
                currentChildIndex++;
                continue;
            }

            BaseCardClass card = child.GetComponent<CardContainer>().card;
            GameObject cardGameObject = card.gameObject;
            
            trustChange += card.trust;
            supportChange += card.support;

            timeChange -= card.cost;

            // Move to next card if there's no effects on the current one.
            if (!card.hasEffect)
            {
                currentChildIndex++;
                continue;
            }

            bool othersHaveRequiredCategory = false;
            bool requirementsAreMet = false;


            #region "Friendly CategoryCheck"
            if (card.isAffectedByFriendly && card.friendlyCategoryAffectedBy.Length > 0)
            {
                int oppositeCard = (currentChildIndex == 0) ? 1 : 0;
                CardContainer oppositeCardContainer = currentPlayedCards.transform
                    .GetChild(oppositeCard).GetComponent<CardContainer>();

                if (oppositeCardContainer.empty)
                {
                    currentChildIndex++;
                    continue;
                }

                // If only let's say 1, 2 then go through twice checking if other card == 1 || 2.
                foreach (BaseCardClass.Category a in card.friendlyCategoryAffectedBy)
                {
                    othersHaveRequiredCategory |= oppositeCardContainer.card.category == a;
                }
            }
            #endregion
            #region "Enemy CategoryCheck"
            else if (!card.isAffectedByFriendly && card.enemyCategoryAffectedBy.Length > 0)
            {
                foreach (Transform containerTransform in opponentPlayedCards.transform)
                {
                    CardContainer container = containerTransform.GetComponent<CardContainer>();

                    if (container.empty) continue;

                    foreach (BaseCardClass.Category a in card.enemyCategoryAffectedBy)
                    {
                        othersHaveRequiredCategory |= container.card.category == a;
                    }
                }
            }
            #endregion
            else
            {
                // There's no requirements for other categories.
                othersHaveRequiredCategory = true;
            }

            if (!othersHaveRequiredCategory)
            {
                currentChildIndex++;
                continue;
            }

            #region "Others Requirement Check"
            switch (card.requirements)
            {
                case BaseCardClass.Requirements.NONE:
                    requirementsAreMet = true;
                    break;

                case BaseCardClass.Requirements.TRUST_SELF_LOWER_THAN_LIMIT:
                    requirementsAreMet = boardManager.trustEnemy < card.trustSelfLowerThanLimit;
                    break;

                case BaseCardClass.Requirements.TRUST_SELF_LARGER_THAN_LIMIT:
                    requirementsAreMet = boardManager.trustEnemy > card.trustSelfLargerThanLimit;
                    break;

                case BaseCardClass.Requirements.TRUST_SELF_LOWER_THAN_ENEMY:
                    requirementsAreMet = boardManager.trustEnemy < boardManager.trustPlayer;
                    break;

                case BaseCardClass.Requirements.TRUST_SELF_LARGER_THAN_ENEMY:
                    requirementsAreMet = boardManager.trustEnemy > boardManager.trustPlayer;
                    break;

                case BaseCardClass.Requirements.SUPPORT_SELF_LOWER_THAN_LIMIT:
                    requirementsAreMet = boardManager.supportSliderValue < card.supportSelfLowerThanLimit;
                    break;

                case BaseCardClass.Requirements.SUPPORT_SELF_LARGER_THAN_LIMIT:
                    requirementsAreMet = boardManager.supportSliderValue > card.supportSelfLargerThanLimit;
                    break;

            }
            #endregion
            requirementsMet.Add(requirementsAreMet);
            requirementsLocalArray[currentChildIndex] = requirementsAreMet;

            if (!requirementsAreMet)
            {
                currentChildIndex++;
                continue;
            }

            #region "Effects Do If Requirements Are Met"
            switch (card.effects)
            {
                case BaseCardClass.Effects.ADD_SUPPORT_SELF:
                    supportChange += card.supportOnEffect;
                    break;

                case BaseCardClass.Effects.ADD_TRUST_SELF:
                    trustChange += card.trustOnEffect;
                    break;

                case BaseCardClass.Effects.ADD_TRUST_SUPPORT_SELF:
                    trustChange += card.trustOnEffect;
                    supportChange += card.supportOnEffect;
                    break;

                case BaseCardClass.Effects.ADD_TRUST_ENEMY:
                    trustChangePlayer += card.trust;
                    break;

                case BaseCardClass.Effects.REMOVE_TRUST_ENEMY:
                    trustChangePlayer -= card.trust;
                    break;

                case BaseCardClass.Effects.ADD_SUPPORT_SELF_ADD_TRUST_ENEMY:
                    supportChange += card.supportOnEffect;
                    trustChangePlayer += card.trustOnEffect;
                    break;

                case BaseCardClass.Effects.GAIN_PERCENTAGE_OF_DIFFERENCE:
                    int enemySupport = (int)boardManager.supportSlider.maxValue - boardManager.supportSliderValue;
                    int difference = Mathf.Abs(enemySupport - boardManager.supportSliderValue);
                    int supportToAdd = difference * (card.percentOnEffect / 100);

                    supportChange += supportToAdd;
                    break;

            }



            #endregion
        }

        for (int i = 0; i < 7; i++)
        {
            requirementsMet.Add(requirementsLocalArray[i]);
        }

        //return
        CardCombination thisCombination = new CardCombination();
        thisCombination.supportChange = supportChange;
        thisCombination.trustChange = trustChange;
        thisCombination.trustChangePlayer = trustChangePlayer;
        thisCombination.timeChange = timeChange;
        thisCombination.Card1 = currentPlayedCards.transform.GetChild(0).gameObject;
        thisCombination.Card2 = currentPlayedCards.transform.GetChild(1).gameObject;
        return thisCombination;
    }
}
