﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

    public GameObject board;
    public GameObject hand;
    public GameObject playerPlayedCards;

    public int currentScore;
    public int bestScore;
    public int secondBestScore;
    public BaseCardClass currentCard;
    public BaseCardClass cardToPlay;
    public BaseCardClass secondCardToPlay;

    public bool othersHaveRequiredCategory;
    public bool requirementsAreMet;

    public List<bool> requirementsMet = new List<bool>();
    public List<CardContainer> chosenCards = new List<CardContainer>();




    public void ChooseACard()
    {
        int currentChildIndex = 0;

        requirementsMet.Clear();
        chosenCards.Clear();
        currentCard = null;
        cardToPlay = null;
        secondCardToPlay = null;

        #region "Checking Reqirements"
        foreach (Transform child in hand.transform)
        {
            BaseCardClass card = child.GetComponent<CardContainer>().card;
            GameObject cardGameObject = card.gameObject;

            othersHaveRequiredCategory = false;
            requirementsAreMet = false;

            if (child.childCount == 0)
            {
                currentChildIndex++;
                
                continue;
            }

            if (!card.hasEffect)
            {
                currentChildIndex++;
                requirementsMet.Add(true);
                continue;
            }


            if (!card.isAffectedByFriendly && card.enemyCategoryAffectedBy.Length > 0)
            {
                foreach (Transform containerTransform in playerPlayedCards.transform)
                {
                    CardContainer container = containerTransform.GetComponent<CardContainer>();

                    if (container.empty) continue;

                    foreach (BaseCardClass.Category a in card.enemyCategoryAffectedBy)
                    {
                        othersHaveRequiredCategory |= container.card.category == a;
                    }
                }
            }
            else 
            {
                othersHaveRequiredCategory = true;
            }

            if (!othersHaveRequiredCategory)
            {
                currentChildIndex++;
                requirementsMet.Add(false);
                continue;
            }

            switch (card.requirements)
            {
                case BaseCardClass.Requirements.NONE:
                    requirementsAreMet = true;
                    break;

                case BaseCardClass.Requirements.TRUST_SELF_LOWER_THAN_LIMIT:
                    requirementsAreMet = (board.GetComponent<BoardManager>().trustEnemy < card.trustSelfLowerThanLimit);
                    break;

                case BaseCardClass.Requirements.TRUST_SELF_LARGER_THAN_LIMIT:
                    requirementsAreMet = (board.GetComponent<BoardManager>().trustEnemy > card.trustSelfLargerThanLimit);
                    break;

                case BaseCardClass.Requirements.TRUST_SELF_LARGER_THAN_ENEMY:
                    requirementsAreMet = (board.GetComponent<BoardManager>().trustEnemy > board.GetComponent<BoardManager>().trustPlayer);
                    break;

                case BaseCardClass.Requirements.SUPPORT_SELF_LOWER_THAN_LIMIT:
                    requirementsAreMet = (board.GetComponent<BoardManager>().supportSliderValue > card.supportSelfLowerThanLimit);
                    break;

                case BaseCardClass.Requirements.SUPPORT_SELF_LARGER_THAN_LIMIT:
                    requirementsAreMet = (board.GetComponent<BoardManager>().supportSliderValue > card.supportSelfLargerThanLimit);
                    break;

            }
            requirementsMet.Add(requirementsAreMet);
        }
        #endregion

        #region "Checking Score"
        bestScore = 0;
        currentScore = 0;
        for (int i = 0; i < hand.transform.childCount; i++)
        {

            if (hand.transform.GetChild(i).childCount > 0)
            {
                currentScore = hand.transform.GetChild(i).transform.GetChild(0).GetComponent<BaseCardClass>().trust + hand.transform.GetChild(i).transform.GetChild(0).GetComponent<BaseCardClass>().support;
                currentCard = hand.transform.GetChild(i).GetComponent<CardContainer>().card;


                if (currentCard.hasEffect)
                {
                    switch (currentCard.requirements)
                    {
                        case BaseCardClass.Requirements.NONE:
                            Debug.Log(i);
                            currentScore += currentCard.GetComponent<BaseCardClass>().supportOnEffect;
                            break;

                        case BaseCardClass.Requirements.TRUST_SELF_LOWER_THAN_LIMIT:
                            if (board.GetComponent<BoardManager>().requirementsMet[i])
                            {
                                Debug.Log(i);
                                currentScore += currentCard.GetComponent<BaseCardClass>().supportOnEffect;
                            }
                            break;

                        case BaseCardClass.Requirements.TRUST_SELF_LARGER_THAN_LIMIT:
                            if (board.GetComponent<BoardManager>().requirementsMet[i])
                            {
                                Debug.Log(i);
                                currentScore += currentCard.GetComponent<BaseCardClass>().supportOnEffect;
                            }
                            break;

                        case BaseCardClass.Requirements.TRUST_SELF_LARGER_THAN_ENEMY:
                            if (board.GetComponent<BoardManager>().requirementsMet[i])
                            {
                                Debug.Log(i);
                                currentScore += currentCard.GetComponent<BaseCardClass>().supportOnEffect;
                            }
                            break;

                        case BaseCardClass.Requirements.SUPPORT_SELF_LOWER_THAN_LIMIT:
                            if (board.GetComponent<BoardManager>().requirementsMet[i])
                            {
                                Debug.Log(i);
                                currentScore += currentCard.GetComponent<BaseCardClass>().supportOnEffect;
                            }
                            break;

                        case BaseCardClass.Requirements.SUPPORT_SELF_LARGER_THAN_LIMIT:
                            if (board.GetComponent<BoardManager>().requirementsMet[i])
                            {
                                Debug.Log(i);
                                currentScore += currentCard.GetComponent<BaseCardClass>().supportOnEffect;
                            }
                            break;

                    }

                }


            }

            if (currentScore >= bestScore)
            {
                secondCardToPlay = cardToPlay;
                cardToPlay = currentCard;
                secondBestScore = bestScore;
                bestScore = currentScore;

                if (secondCardToPlay == null)
                {
                    secondCardToPlay = hand.transform.GetChild(Random.Range(1, 6)).GetComponent<CardContainer>().card;
                }
            }
        }
        #endregion

        chosenCards.Add(cardToPlay.GetComponentInParent<CardContainer>());
        chosenCards.Add(secondCardToPlay.GetComponentInParent<CardContainer>());
    }
}
