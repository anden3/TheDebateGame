﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderUpdater : MonoBehaviour {

    [Header("True = Sound, False = Music")]
    public bool soundOrMusic;

    private void OnEnable()
    {
        if (soundOrMusic)
        {
            GetComponent<Slider>().value = SoundManager.soundVolume;
        }
        else
        {
            GetComponent<Slider>().value = MusicManager.musicVolume;
        }
    }

}
