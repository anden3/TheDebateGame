﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingStarController : MonoBehaviour
{
    public float startSpeed = 0.2f;
    public float endSpeed = 1f;
    public float increaseSpeedPerFrame = 0.05f;
    [Space]
    public float endSize = 0.1f;
    public float endSizeChangePerFrame = 0.02f;
    [Space]
    public float destroyWhenDistanceCloserThan = 0.1f;

    [System.NonSerialized]
    public Transform startPosition;
    [System.NonSerialized]
    public Transform targetPosition;

    private float speed;
    private float size;
    
	void Start()
    {
        speed = startSpeed;
        size = transform.localScale.x;
        transform.position = startPosition.position;
	}
	
	void FixedUpdate() {
        float distanceTarget = Vector3.Distance(targetPosition.position, transform.position);

        if (distanceTarget < destroyWhenDistanceCloserThan)
        {
            EventManager.instance.RunEvent("DestroyShootingStar");
            Destroy(gameObject);
        }

        transform.position = Vector3.MoveTowards(
            transform.position, targetPosition.position,
            speed * Time.fixedDeltaTime
        );
        transform.localScale = new Vector2(size, size);

        if (speed < endSpeed) speed += increaseSpeedPerFrame;
        if (size  > endSize ) size  -= endSizeChangePerFrame;
    }
}
