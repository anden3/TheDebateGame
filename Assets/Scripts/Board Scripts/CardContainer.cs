﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardContainer : MonoBehaviour
{
    static public float HOVER_OFFSET_Y = 0;
    static public Transform BOARD_HOVER_POSITION;


    static bool cardIsPickedUp = false;


    public enum ContainerLocation {
        PLAYER_HAND,
        PLAYER_BOARD,
        ENEMY_HAND,
        ENEMY_BOARD,
        INSPECTOR_DECK,
        PLAYER_DRAG
    };

    public ContainerLocation containerLocation;
    public BaseCardClass card = null;
    public bool empty = true;

    void Update()
    {
        empty = transform.childCount == 0;

        if (!empty)
        {
            card = transform.GetChild(0).GetComponent<BaseCardClass>();
        }
    }

    void OnMouseEnter()
    {
        if (empty) return;

        if (!BoardManager.boardInteractable && containerLocation != ContainerLocation.INSPECTOR_DECK) return;

        // Don't zoom cards if you're already holding one.
        if (cardIsPickedUp) return;

        switch (containerLocation)
        {
            case ContainerLocation.PLAYER_HAND:
                card.Hover(new Vector3(0, HOVER_OFFSET_Y));

                EventManager.instance.RunEvent("CardHoverEnter");

                //TODO: Implement better way to play random sound.
                EventManager.instance.RunEvent("CardHoverEnter" + Random.Range(1, 3));
                break;
            
            case ContainerLocation.ENEMY_BOARD:
            case ContainerLocation.PLAYER_BOARD:
                card.HoverWhilePlayed(BOARD_HOVER_POSITION);

                EventManager.instance.RunEvent("CardHoverEnterPlayed");
                break;
        }
    }

    void OnMouseOver()
    {
        // Make sure a card has actually been picked up.
        if (cardIsPickedUp)
        {
            if (!Input.GetMouseButtonUp(0)) return;

            switch (containerLocation)
            {
                case ContainerLocation.PLAYER_HAND:
                    cardIsPickedUp = false;

                    BoardManager.instance.SetBoardInteractable(true);

                    BoardManager.instance.CardReturnPlayer(
                        BoardManager.instance.containerOnMouse
                    );
                    break;

                case ContainerLocation.PLAYER_BOARD:
                    cardIsPickedUp = false;

                    BoardManager.instance.SetBoardInteractable(true);

                    BoardManager.instance.CardPlacePlayer(
                        BoardManager.instance.containerOnMouse
                    );

                    break;
            }

            // Forgive me father, for I have sinned.
            Update();
            OnMouseEnter();
        }
    }

    void OnMouseExit()
    {
        if (empty) return;
        if (cardIsPickedUp) return;

        switch (containerLocation)
        {
            case ContainerLocation.PLAYER_HAND:
                card.StopHover();
                EventManager.instance.RunEvent("CardHoverExit");
                break;

            case ContainerLocation.ENEMY_BOARD:
            case ContainerLocation.PLAYER_BOARD:
                card.StopHoverWhilePlayed();

                EventManager.instance.RunEvent("CardHoverExitPlayed");
                break;
        }
    }

    // TODO: Fix ugly shit.
    void OnMouseDown()
    {
        if (empty) return;
        if (cardIsPickedUp) return;
        if (!BoardManager.instance.playersTurn) return;

        if (BoardManager.instance.hasPlayedSpecialEffect && card.category == BaseCardClass.Category.STRATEGY && BoardManager.instance.inspectorDeck.transform.GetChild(0).childCount == 0)
        {
            EventManager.instance.RunEvent("FailClickSpecial");
            return;
        }
        if (!BoardManager.boardInteractable && containerLocation != ContainerLocation.INSPECTOR_DECK) return;

        switch (containerLocation)
        {
            case ContainerLocation.PLAYER_BOARD:
            case ContainerLocation.PLAYER_HAND:
                empty = true;
                cardIsPickedUp = true;

                BoardManager.instance.SetBoardInteractable(false);
                card.Pickup();
                break;
            
            case ContainerLocation.INSPECTOR_DECK:
                BoardManager.instance.SelectCardInspector(this);
                break;

            
        }
    }

    // This method gets called after mouse button has been released,
    // no matter where the mouse actually is.
    // Therefore, if the mouse button has been released and the card hasn't been placed afterwards,
    // it must have been misplaced and can therefore return to the hand.
    IEnumerator OnMouseUp()
    {
        // Wait one frame to ensure OnMouseOver() has been ran.
        yield return new WaitForEndOfFrame();

        if (!cardIsPickedUp) yield break;

        cardIsPickedUp = false;

        //BoardManager.instance.SetBoardInteractable(true);
        //card.PlaceInHand(transform, false);

        switch (containerLocation)
        {
            case ContainerLocation.PLAYER_HAND:
                cardIsPickedUp = false;

                BoardManager.instance.SetBoardInteractable(true);

                BoardManager.instance.CardReturnPlayer(
                    BoardManager.instance.containerOnMouse
                );
                break;

            case ContainerLocation.PLAYER_BOARD:
                cardIsPickedUp = false;

                BoardManager.instance.SetBoardInteractable(true);

                BoardManager.instance.CardPlacePlayer(
                    BoardManager.instance.containerOnMouse
                );

                break;
        }
    }
}
