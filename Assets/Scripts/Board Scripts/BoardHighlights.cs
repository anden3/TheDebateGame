﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardHighlights : MonoBehaviour {

    public GameObject highLight1;
    public GameObject highLight2;

    public GameObject playSpaces;
	// Use this for initialization
	void Start ()
    {
        highLight1.SetActive(false);
        highLight2.SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {

        if (!gameObject.GetComponent<CardContainer>().empty && playSpaces.transform.GetChild(0).childCount == 0)
        {
            highLight1.SetActive(true);
        }
        else
        {
            highLight1.SetActive(false);
        }

        if (!gameObject.GetComponent<CardContainer>().empty && playSpaces.transform.GetChild(1).childCount == 0)
        {
            highLight2.SetActive(true);
        }
        else
        {
            highLight2.SetActive(false);
        }
    }
}
