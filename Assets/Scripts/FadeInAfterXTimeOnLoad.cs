﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInAfterXTimeOnLoad : MonoBehaviour {
    [Header("Works for Image gui")]
    public float waitFor = 10f;
    public float pausePerIncrease = 0.02f;
    public float increaseOfOpacityPerStep = 0.01f;
    public float maxOpacity = 0.4f;
    // Use this for initialization
    void Start()
    {
        GetComponent<Image>().color = new Color(1, 1, 1, 0);
        GetComponent<Button>().interactable = false;
        StartCoroutine(TimerFunction());
    }

    IEnumerator TimerFunction()
    {
        yield return new WaitForSeconds(waitFor);
        GetComponent<Button>().interactable = true;
        for (float f = 0; f <= maxOpacity; f += increaseOfOpacityPerStep)
        {
            GetComponent<Image>().color = new Color(1, 1, 1, f);
            yield return new WaitForSeconds(pausePerIncrease);
        }
        GetComponent<Image>().color = new Color(1, 1, 1, maxOpacity);
    }
}
