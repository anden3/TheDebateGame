﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCardClass : MonoBehaviour
{
    static public float DEFAULT_ZOOM = 0;
    static public float PLAYED_ZOOM = 0;
    static public float HOVER_ZOOM = 0;
    static public float PLAYED_HOVER_ZOOM = 1;
    static public float INSPECTOR_ZOOM = 0;

    [System.Serializable]
    public enum Category
    {
        PURE_LOGOS = 1,
        CORRUPT_LOGOS = 2,
        PURE_PATHOS = 3,
        CORRUPT_PATHOS = 4,
        STRATEGY = 5,
        GAME_LOGIC = 6
    }

    public enum Requirements
    {
        NONE,
        TRUST_SELF_LOWER_THAN_LIMIT,
        TRUST_SELF_LARGER_THAN_LIMIT,
        TRUST_SELF_LOWER_THAN_ENEMY,
        TRUST_SELF_LARGER_THAN_ENEMY,
        SUPPORT_SELF_LOWER_THAN_LIMIT,
        SUPPORT_SELF_LARGER_THAN_LIMIT,
    }

    public enum Effects
    {
        ADD_SUPPORT_SELF,
        ADD_TRUST_SELF,
        ADD_TRUST_SUPPORT_SELF,
        ADD_TRUST_ENEMY,
        REMOVE_TRUST_ENEMY,
        ADD_SUPPORT_SELF_ADD_TRUST_ENEMY,
        GAIN_PERCENTAGE_OF_DIFFERENCE,
    }
    public enum SpecialEffects
    {
        DRAW_NEW_HAND,
        SEARCH_TOP_X_IN_STACK_FOR_Y_CARDS,
        BRING_BACK_X_CARDS_FROM_DISCARDED,
        LOCK_ONE_CARD_POSITION_BOTH,
    }

    public SpecialEffects specialEffect;
    public int cardsTopStackViewCount;
    public int cardsFromTopStackCount;
    public int cardsFromDiscardPileCount;

    public Category category;
    public int trust;
    public int support;
    public float cost;

    public bool hasEffect;
    public bool isAffectedByFriendly;

    public Category[] friendlyCategoryAffectedBy;
    public Category[] enemyCategoryAffectedBy;

    public Requirements requirements;

    public int trustSelfLowerThanLimit;
    public int trustSelfLargerThanLimit;
    public int supportSelfLowerThanLimit;
    public int supportSelfLargerThanLimit;

    public Effects effects;

    public int trustOnEffect;
    public int supportOnEffect;
    public int percentOnEffect;

    // Component references.
    private SpriteRenderer sr = null;

    // Extern references.
    private Transform container = null;
    private GameObject zoomedCard;

    // Magic numbers.
    private readonly int SORTING_ORDER_DEFAULT = 4;
    private readonly int SORTING_ORDER_HOVER_PLAYED = 4;
    private readonly int SORTING_ORDER_HOVER = 6;
    private readonly int SORTING_ORDER_INSPECTOR = 11;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    public void PlaceInHand(Transform container, bool isFillDeck)
    {
        if (zoomedCard != null)
        {
            Destroy(zoomedCard);
        }

        this.container = container;

        gameObject.SetActive(true);
        transform.SetParent(container);
        transform.position = container.position;
        transform.localScale = new Vector2(DEFAULT_ZOOM, DEFAULT_ZOOM);

        sr.sortingOrder = SORTING_ORDER_DEFAULT;

        if (container.parent.name == "PlayerDeck" && isFillDeck)
        {
            sr.color = new Color(1, 1, 1, 0);
            StartCoroutine(CreateMovingIntoPlayerHand(container.position, GameObject.Find("PlayerStackOfCards").GetComponent<Transform>().position));
        }
    }

    public void PlaceInDeck(Transform deck)
    {
        transform.SetParent(deck);
        gameObject.SetActive(false);
    }

    public void ShowInInspector(Transform container)
    {
        gameObject.SetActive(true);
        transform.SetParent(container);
        transform.position = container.position;

        //set sprite layer to be 11 so its ontop of new
        sr.sortingOrder = SORTING_ORDER_INSPECTOR;
        transform.localScale = new Vector2(INSPECTOR_ZOOM, INSPECTOR_ZOOM);
    }
    public void MoveToBoard(Transform container)
    {
        this.container = container;
        sr.sortingOrder = SORTING_ORDER_DEFAULT;

        transform.SetParent(container);
        transform.position = container.position;
        transform.localScale = new Vector2(PLAYED_ZOOM, PLAYED_ZOOM);

        container.GetComponent<CardContainer>().card = this;
        container.GetComponent<CardContainer>().empty = false;

        if (!BoardManager.instance.playersTurn)
        {
            sr.color = new Color(1, 1, 1, 0);
            StartCoroutine(CreateOpponentMoveIntoPlayed(container.position, new Vector3(0, 12, 0)));
        }
    }

    public void Discard(Transform discardPile)
    {
        if (zoomedCard != null)
        {
            Destroy(zoomedCard);
        }

        CreateDiscardedMovingCard(discardPile.position, transform.position);

        transform.SetParent(discardPile);
        transform.position = discardPile.position;
        gameObject.SetActive(false);
    }

    public void Hover(Vector3 positionOffset)
    {
        sr.sortingOrder = SORTING_ORDER_HOVER;
        transform.localScale = new Vector2(HOVER_ZOOM, HOVER_ZOOM);
        transform.position += positionOffset;
    }

    public void StopHover()
    {
        sr.sortingOrder = SORTING_ORDER_DEFAULT;
        transform.localScale = new Vector2(DEFAULT_ZOOM, DEFAULT_ZOOM);

        // TODO: Find out why this is sometimes null.
        transform.position = container.position;
    }

    public void HoverWhilePlayed(Transform displayPosition)
    {
        zoomedCard = Instantiate(gameObject);
        zoomedCard.GetComponent<SpriteRenderer>().sortingOrder = SORTING_ORDER_HOVER_PLAYED;

        zoomedCard.transform.position = displayPosition.position;
        zoomedCard.transform.localScale = new Vector2(
            PLAYED_HOVER_ZOOM, PLAYED_HOVER_ZOOM
        );
    }

    public void StopHoverWhilePlayed()
    {
        Destroy(zoomedCard);
    }

    public void Pickup()
    {
        if (zoomedCard != null)
        {
            Destroy(zoomedCard);
        }

        sr.sortingOrder = SORTING_ORDER_HOVER;
        transform.localScale = new Vector2(HOVER_ZOOM, HOVER_ZOOM);

        transform.SetParent(
            BoardManager.instance.containerOnMouse.transform
        );
        transform.localScale = new Vector2(0.25f, 0.25f);
        transform.position = transform.parent.position;
        sr.sortingOrder = 10;
    }

    private void CreateDiscardedMovingCard(Vector3 targetPosition, Vector3 currentPos)
    {
        GameObject cardTemp = (GameObject)Instantiate(Resources.Load("CardMovingToDiscarded"));
        cardTemp.GetComponent<SpriteRenderer>().sprite = sr.sprite;
        cardTemp.GetComponent<SpriteRenderer>().sortingOrder = 5;
        cardTemp.transform.parent = null;

        CardMoveFadeAwayFromPosToPos thing = cardTemp.GetComponent<CardMoveFadeAwayFromPosToPos>();
        thing.startPos = currentPos;
        thing.endPos = targetPosition;

        EventManager.instance.RunEvent("CreateCardToDiscarded");
    }

    IEnumerator CreateOpponentMoveIntoPlayed(Vector3 targetPosition, Vector3 currentPos)
    {
        GameObject cardTemp = (GameObject)Instantiate(Resources.Load("OpponentCardMovingIntoPlayed"));
        cardTemp.GetComponent<SpriteRenderer>().sprite = sr.sprite;
        cardTemp.GetComponent<SpriteRenderer>().sortingOrder = 5;
        cardTemp.transform.parent = null;

        CardMoveFadeToPosFromPosPlusX thing = cardTemp.GetComponent<CardMoveFadeToPosFromPosPlusX>();
        thing.startPos = currentPos;
        thing.endPos = targetPosition;

        EventManager.instance.RunEvent("CreateOpponentMoveIntoPlayed");

        //once it gone, do place
        yield return new WaitUntil(() => cardTemp == null);

        sr.color = new Color(1, 1, 1, 1);
    }

    IEnumerator CreateMovingIntoPlayerHand(Vector3 targetPosition, Vector3 currentPos)
    {
        GameObject cardTemp = (GameObject)Instantiate(Resources.Load("PlayerStackToHand"));
        cardTemp.GetComponent<SpriteRenderer>().sprite = sr.sprite;
        cardTemp.GetComponent<SpriteRenderer>().sortingOrder = 5;
        cardTemp.transform.parent = null;

        CardMoveFadeToPosFromPosPlusX thing = cardTemp.GetComponent<CardMoveFadeToPosFromPosPlusX>();
        thing.startPos = currentPos;
        thing.endPos = targetPosition;

        EventManager.instance.RunEvent("CreateCardIntoHand");

        //once it gone, do place
        yield return new WaitUntil(() => cardTemp == null);

        sr.color = new Color(1, 1, 1, 1);
    }
}
