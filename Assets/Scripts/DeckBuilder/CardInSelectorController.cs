﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardInSelectorController : MonoBehaviour
{
    private SpriteRenderer sr;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    void OnMouseUp()
    {
        if (sr.sprite == null) return;
        if (DeckSelectorManager.instance.previewCardOverlayEnabled) return;
        if (DeckSelectorManager.instance.isInsideNonClickable) return;
        if (!DeckSelectorManager.instance.canDisplayPreview) return;

        DeckSelectorManager.instance.previewCardOverlay.GetComponent<SpriteRenderer>().sprite = sr.sprite;
        DeckSelectorManager.instance.previewCardOverlayEnabled = true;

        EventManager.instance.RunEvent("OpenInspectCard");
    }
}
