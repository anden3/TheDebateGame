﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectDeckController : MonoBehaviour
{
    public float yOffset = 0;
    private DeckOfCards deck;

    void Awake()
    {
        deck = GetComponent<DeckOfCards>();
    }

    void OnMouseUp()
    {
        if (deck == null) return;
        if (DeckSelectorManager.instance.previewCardOverlayEnabled) return;
        if (DeckSelectorManager.instance.isInsideNonClickable) return;
        if (!DeckSelectorManager.instance.canDisplayPreview) return;

        DeckSelectorManager.instance.selectedDeck = deck;

        GameObject deckSec = DeckSelectorManager.instance.outlineSelectedDeck;
        deckSec.transform.SetParent(transform);
        deckSec.transform.position = new Vector2(transform.position.x, (transform.position.y + this.yOffset));

        EventManager.instance.RunEvent("SelectNewDeck");
    }
}
