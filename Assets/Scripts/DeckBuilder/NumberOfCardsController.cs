﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberOfCardsController : MonoBehaviour
{
    public int cardCount;
    public Sprite[] spritesToUse;

    private SpriteRenderer sr;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        sr.sprite = null;
        transform.localScale = new Vector3(1.5f, 1.5f);
    }

    void Update ()
    {
        cardCount = Mathf.Clamp(cardCount, 0, 4);

        sr.sprite = (cardCount >= 1) ?
            spritesToUse[cardCount - 1] :
            null;
	}
}
