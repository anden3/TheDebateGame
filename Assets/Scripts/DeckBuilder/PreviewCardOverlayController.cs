﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewCardOverlayController : MonoBehaviour
{
    void OnMouseUp()
    {
        if (DeckSelectorManager.instance.previewCardOverlayEnabled)
        {
            DeckSelectorManager.instance.previewCardOverlayEnabled = false;
            EventManager.instance.RunEvent("CloseInspectCard");
        }
    }
}
