﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckSelectorManager : MonoBehaviour
{
    public static DeckSelectorManager instance = null;

    public GameObject outlineSelectedDeck;
    public GameObject previewCardOverlay;
    public GameObject continueButton;
    public GameObject backButton;
    public GameObject nonClickableArea;

    // Order of children should be the same as in BaseCardClass.Category.
    public GameObject scrollViewCards;

    [Header("Deck being displayed")]
    public DeckOfCards selectedDeck = null;

    [HideInInspector]
    public bool isInsideNonClickable;
    [HideInInspector]
    public bool previewCardOverlayEnabled = false;

    private string previousSelectedDeckName;

    private List<GameObject> categoryLines = new List<GameObject>();

    private int furthestKiddo = 0;
    private int stepPerKiddo = 1625 / 5;

    [System.NonSerialized]
    public bool canDisplayPreview = true;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
    }

    void Start()
    {
        categoryLines = scrollViewCards.transform.Cast<Transform>().Select(
            c => c.gameObject
        ).ToList();

        SetScrollCardsAsDefault();

        if (selectedDeck != null)
        {
            DisplaySelectedDeck(selectedDeck);
        }

        canDisplayPreview = true;
    }

    void Update()
    {
        continueButton.GetComponent<Button>().interactable = !previewCardOverlayEnabled;
        backButton.GetComponent<Button>().interactable = !previewCardOverlayEnabled;
        previewCardOverlay.SetActive(previewCardOverlayEnabled);

        CheckIfInsideNonClickable();

        // Deck has changed.
        if (selectedDeck && selectedDeck.name != previousSelectedDeckName)
        {
            SetScrollCardsAsDefault();
            DisplaySelectedDeck(selectedDeck);
        }
    }

    void CheckIfInsideNonClickable()
    {
        
        isInsideNonClickable = false;

        foreach (BoxCollider2D col in nonClickableArea.GetComponentsInChildren<BoxCollider2D>())
        {
            if (col.bounds.Contains(Camera.main.ScreenToWorldPoint(Input.mousePosition)))
            {
                isInsideNonClickable = true;
                return;
            }
        }
    }

    void DisplaySelectedDeck(DeckOfCards deckToDisplay)
    {
        foreach (GameObject a in deckToDisplay.arrayOfCards)
        {
            if (a == null || a.GetComponent<BaseCardClass>() == null) continue;
            PlaceCardInCategory(a);
        }

        previousSelectedDeckName = selectedDeck.name;

        furthestKiddo = 0;
        for (int i = 0; i < categoryLines.Count; i++)
        {
            int linesInCategory = categoryLines[i].transform.Cast<Transform>().Where(
                c => c.GetChild(0).GetComponent<NumberOfCardsController>().cardCount > 0
            ).Count();
            if (linesInCategory > furthestKiddo) furthestKiddo = linesInCategory;
        }

        scrollViewCards.GetComponentInParent<RectTransform>().sizeDelta = new Vector2(0, furthestKiddo * stepPerKiddo);

        scrollViewCards.transform.parent.parent.parent.GetChild(1).GetComponent<Scrollbar>().value = 1;
    }

    void PlaceCardInCategory(GameObject cardPrefab)
    {
        GameObject categoryLine = categoryLines[
            (int)cardPrefab.GetComponent<BaseCardClass>().category - 1
        ];
        
        foreach (Transform child in categoryLine.transform)
        {
            GameObject currentPos = child.gameObject;

            // Check if current position is empty.
            if (currentPos.name.StartsWith("CardPos"))
            {
                currentPos.name = cardPrefab.name;
                currentPos.GetComponent<SpriteRenderer>().sprite = cardPrefab.GetComponent<SpriteRenderer>().sprite;
            }
            // If current position is not empty, make sure it's the same type as the current card.
            else if (currentPos.name != cardPrefab.name)
            {
                continue;
            }

            currentPos.transform.GetChild(0).GetComponent<NumberOfCardsController>().cardCount++;
            break;
        }
    }

    void SetScrollCardsAsDefault()
    {
        foreach (GameObject category in categoryLines)
        {
            int i = 1;

            foreach (Transform position in category.transform)
            {
                GameObject currentCard = position.gameObject;

                currentCard.name = "CardPos" + i++;
                currentCard.GetComponent<SpriteRenderer>().sprite = null;
                currentCard.transform.GetChild(0).GetComponent<NumberOfCardsController>().cardCount = 0;
            }
        }
    }

    public void SetDeckOfCards()
    {
        EventManager.instance.RunEvent("ClickContinueButton");
        GameManager.instance.GetComponent<DeckOfCards>().arrayOfCards = selectedDeck.arrayOfCards;
    }

    public void SetInteractable(bool whatToBe)
    {
        canDisplayPreview = whatToBe;
        backButton.GetComponent<Button>().enabled = whatToBe;
        continueButton.GetComponent<Button>().enabled = whatToBe;
    }
}
