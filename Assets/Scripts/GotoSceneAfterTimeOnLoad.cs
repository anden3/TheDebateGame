﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GotoSceneAfterTimeOnLoad : MonoBehaviour {
    public string sceneNameGoTo = "Board1";
    public float waitFor = 10f;
	// Use this for initialization
	void Start () {
        StartCoroutine(TimerFunction());
	}
	
	IEnumerator TimerFunction()
    {
        yield return new WaitForSeconds(waitFor);
        GameManager.instance.GoToScene(sceneNameGoTo);
    }
}
