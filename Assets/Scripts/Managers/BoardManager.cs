﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BoardManager : MonoBehaviour
{
    public static BoardManager instance = null;

    public static bool boardInteractable = true;

    public bool enemyCanPlayCards = true;
    public bool enemyPlaysTwoCards = true;

    [Header("Scenes")]
    public string sceneOnWin = "Board1Victory";
    public string sceneOnLose = "Board1GameOver";

    [Header("Object references")]
    public GameObject shootingStarPrefab;
    public GameObject clockBlinker;
    public CardContainer containerOnMouse;

    [Header("Card stacks")]
    public GameObject playerDeck;
    public GameObject enemyDeck;
    [Space]
    public GameObject playerDeckHand;
    public GameObject enemyDeckHand;
    [Space]
    public GameObject playerPlayedCards;
    public GameObject enemyPlayedCards;
    [Space]
    public GameObject playerDiscardPile;
    public GameObject enemyDiscardPile;
    [Space]
    public GameObject inspectorDeck;
    public GameObject inspectorText;

    [Header("Sprites")]
    public GameObject stopSprite;
    public GameObject checkMark;
    public GameObject dealerHands;

    [Header("Images")]
    public GameObject YouWin_First;
    public GameObject YouWin_Second;
    public GameObject YouLose_First;
    public GameObject YouLose_Second;
    public SpriteRenderer overlay;

    [Header("Text")]
    public Text clockText;
    public Text supportSliderText;

    [Header("Sliders")]
    public Slider supportSlider;
    public GameObject trustSliderPlayer;
    public GameObject trustSliderEnemy;

    [Header("Time settings")]
    public float timeRemaining = 60;

    [Header("Turn settings")]
    public float enemyTurnTime = 2f;
    public bool playersTurn = true;
    [Space]
    public Button nextTurnButton;
    public Sprite[] spritesNextButton = new Sprite[3];
    [Space]
    public GameObject pauseMenu;
    public GameObject pauseButton;
    [Space]
    public GameObject helpMenu;
    public GameObject helpButton;
    [Space]
    public GameObject groupImage;
    public GameObject groupImageButton;
    [Space]
    public GameObject yourTurnThing;
    public GameObject enemyTurnThing;

    [Header("Enemy start card settings")]
    public bool enableStartCards = true;
    public GameObject[] card1Options;
    public GameObject[] card2Options;

    [Header("Zoom settings")]
    public float zoomNeutral;
    public float zoomNeutralPlayed;
    [Space]
    public float zoomOnHoverMoveUpY;
    public float zoomOnHoverScale;
    public float zoomOnHoverScalePlayed;
    [Space]
    public float zoomOnInspector;
    [Space]
    public Transform objectMoveToWhenZoomPlayed;

    [Header("Trust settings")]
    public int trustMax = 10;

    [HideInInspector]
    public bool pauseMenuIsOpen = false;
    [HideInInspector]
    public bool helpMenuIsOpen = false;
    [HideInInspector]
    public bool groupImageIsOpen = false;
    [HideInInspector]
    public int supportSliderValue = 50;
    [HideInInspector]
    public int trustPlayer = 0;
    [HideInInspector]
    public int trustEnemy = 0;

    [System.NonSerialized]
    private float pauseTimeBeforeRound;
    
    private bool hasPassedTen = false;
    [System.NonSerialized]
    public bool hasPlayedSpecialEffect = false;
    private bool enemyHasPlayedSpecialEffect = false;

    private bool gameOver = false;

    private bool isRunningFirstStageSupport = false;

    public bool othersHaveRequiredCategory;
    public bool requirementsAreMet;

    private int isLockedPositions = 0;
    
    private GameObject stopSpritePlayer;
    private GameObject stopSpriteEnemy;

    private bool hasChosenCard = false;
    private int inspectorSelectedCard;

    //public bool[] requirementsMet;
    public List<bool>requirementsMet = new List<bool>();

    // Component cache.
    private RectTransform sliderTransform;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        sliderTransform = supportSliderText.GetComponent<RectTransform>();

        // The player gets their cards from GameManager, not via playerDeck.
        DumpArrayIntoGameObject(
            GameManager.instance.GetComponent<DeckOfCards>().arrayOfCards,
            playerDeck
        );
        DumpArrayIntoGameObject(
            enemyDeck.GetComponent<DeckOfCards>().arrayOfCards,
            enemyDeck    
        );
    }

    void Start()
    {
        BaseCardClass.DEFAULT_ZOOM = zoomNeutral;
        BaseCardClass.PLAYED_ZOOM = zoomNeutralPlayed;
        BaseCardClass.HOVER_ZOOM = zoomOnHoverScale;
        BaseCardClass.PLAYED_HOVER_ZOOM = zoomOnHoverScalePlayed;
        BaseCardClass.INSPECTOR_ZOOM = zoomOnInspector;

        CardContainer.HOVER_OFFSET_Y = zoomOnHoverMoveUpY;
        CardContainer.BOARD_HOVER_POSITION = objectMoveToWhenZoomPlayed;

        EventManager.instance.RunEvent("StartBoard1");

        clockText.text = timeRemaining + ":00";

        nextTurnButton.onClick.AddListener(NextTurn);

        FillDeck(playerDeckHand, playerDeck, true);
        FillDeck(enemyDeckHand, enemyDeck, false);

        SetTrustOnSlider(trustSliderPlayer, trustPlayer);
        SetTrustOnSlider(trustSliderEnemy, trustEnemy);

        supportSlider.value = supportSliderValue;

        pauseMenu.SetActive(false);

        inspectorDeck.SetActive(false);
        
        nextTurnButton.GetComponent<Button>().interactable = false;

        Debug.Log("firstHelp: "+GameManager.instance.firstHelp);
        if (GameManager.instance.firstHelp)
        {
            ToggleHelpMenu();
            GameManager.instance.firstHelp = false;
        }
        else
        {
            helpMenuIsOpen = false;
            SetBoardInteractable(true);
        }

        //NextTurn();
        if(enableStartCards) AddTwoCardsOnEnemy();
    }

    void Update()
    {
        supportSliderText.text = supportSlider.value.ToString();
        sliderTransform.position = supportSliderText.GetComponentInParent<RectTransform>().position;

        containerOnMouse.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 12));
    }

    void AddTwoCardsOnEnemy()
    {
        int tempPlace = Random.Range(0, card1Options.Length);
        int tempPlace2 = Random.Range(0, card2Options.Length);
        BaseCardClass card1 = Instantiate(card1Options[tempPlace]).GetComponent<BaseCardClass>();
        BaseCardClass card2 = Instantiate(card2Options[tempPlace2]).GetComponent<BaseCardClass>();
        
        card1.MoveToBoard(enemyPlayedCards.transform.GetChild(0));
        card2.MoveToBoard(enemyPlayedCards.transform.GetChild(1));
    }

    void SetTrustOnSlider(GameObject inputTrustSlider, int inputTrust)
    {
        int i = 0;

        foreach (Transform child in inputTrustSlider.transform)
        {
            child.GetComponent<SpriteRenderer>().enabled = (i++ < inputTrust);
        }

        // TODO: Check if this should be inside the loop above.
        if (inputTrust != 0)
        {
            inputTrustSlider.transform.GetChild(inputTrust - 1).GetComponent<ParticleSystem>().Play();
        }
    }

    void FillDeck(GameObject inputDeck, GameObject inBackStack, bool isPlayerDeck)
    {
        if (inBackStack.transform.childCount > 0)
        {
            int i = 0;

            foreach (Transform child in inputDeck.transform)
            {
                if (child.childCount == 0)
                {
                    Debug.Assert(inBackStack.transform.childCount > 0);

                    BaseCardClass card = inBackStack.transform.GetChild(i).GetComponent<BaseCardClass>();
                    card.PlaceInHand(inputDeck.transform.GetChild(i), true);
                }

                i++;
            }
        }

        (
            isPlayerDeck ? playerDeck : enemyDeck
        ).GetComponent<SpriteRenderer>().enabled = (inBackStack.transform.childCount > 0);

        EventManager.instance.RunEvent(
            isPlayerDeck ? "FillDeckHandPlayer" : "FillDeckHandEnemy"
        );
    }

    void DumpArrayIntoGameObject(GameObject[] arrayIn, GameObject stackBackIn)
    {
        // Knuth shuffle algorithm :: courtesy of Wikipedia :)
        for (int t = 0; t < arrayIn.Length; t++)
        {
            GameObject tmp = arrayIn[t];
            int r = Random.Range(t, arrayIn.Length);
            arrayIn[t] = arrayIn[r];
            arrayIn[r] = tmp;
        }

        foreach (GameObject a in arrayIn)
        {
            GameObject tempObj = Instantiate(a);
            tempObj.SetActive(false);
            tempObj.transform.SetParent(stackBackIn.transform);
        }
    }

    public void CardPlacePlayer(CardContainer container)
    {
        if (!playersTurn) return;
        if (container.empty) return;

        switch (container.card.category)
        {
            case BaseCardClass.Category.GAME_LOGIC:
                // Should never happen.
                Debug.Break();
                return;
            
            case BaseCardClass.Category.STRATEGY:
                if (hasPlayedSpecialEffect)
                {
                    List<Transform> emptySpaces3 = CheckEmptyPositions(playerDeckHand);
                    container.card.MoveToBoard(emptySpaces3[0]);
                    return;
                }

                // TODO: Investigate if card.Discard can be called after all cases.
                switch (container.card.specialEffect)
                {
                    case BaseCardClass.SpecialEffects.DRAW_NEW_HAND:
                        container.card.Discard(playerDiscardPile.transform);
                        hasPlayedSpecialEffect = true;

                        if (playersTurn)
                        {
                            DrawNewHand(playerDeckHand, playerPlayedCards, playerDeck);
                        }
                        else
                        {
                            DrawNewHand(enemyDeckHand, enemyPlayedCards, enemyDeck);
                        }

                        break;

                    case BaseCardClass.SpecialEffects.SEARCH_TOP_X_IN_STACK_FOR_Y_CARDS:
                        if (playerDeck.transform.childCount != 0)
                        {
                            StartCoroutine(DisplayInspector(
                                container.card, playerDeck, playerDeckHand,
                                "TakeFromStackOpenWindow", "TakeFromStackCloseWindow"
                            ));
                            container.card.Discard(playerDiscardPile.transform);
                            hasPlayedSpecialEffect = true;

                        }

                        break;

                    case BaseCardClass.SpecialEffects.BRING_BACK_X_CARDS_FROM_DISCARDED:
                        if (playerDiscardPile.transform.childCount != 0)
                        {
                            StartCoroutine(DisplayInspector(
                                container.card, playerDiscardPile, playerDeckHand,
                                "TakeFromDiscardedOpenWindow", "TakeFromDiscardedCloseWindow"
                            ));
                            container.card.Discard(playerDiscardPile.transform);
                            hasPlayedSpecialEffect = true;
                        }
                        else
                        {
                            List<Transform> emptySpaces3 = CheckEmptyPositions(playerDeckHand);
                            container.card.MoveToBoard(emptySpaces3[0]);
                            return;
                        }

                        break;

                    case BaseCardClass.SpecialEffects.LOCK_ONE_CARD_POSITION_BOTH:
                        container.card.Discard(playerDiscardPile.transform);

                        hasPlayedSpecialEffect = true;
                        isLockedPositions = 2;
                        UpdateLockedPositions();
                        break;
                }
                break;
            
            case BaseCardClass.Category.PURE_LOGOS:
            case BaseCardClass.Category.CORRUPT_LOGOS:
            case BaseCardClass.Category.PURE_PATHOS:
            case BaseCardClass.Category.CORRUPT_PATHOS:
                List<Transform> emptySpaces = CheckEmptyPositions(playerPlayedCards);

                if (emptySpaces.Count == 0)
                {
                    List<Transform> emptySpaces2 = CheckEmptyPositions(playerDeckHand);
                    container.card.MoveToBoard(emptySpaces2[0]);
                    return;
                }
                else
                {
                    container.card.MoveToBoard(emptySpaces[0]);
                    UpdateNextButton(CheckEmptyPositions(playerPlayedCards).Count);
                }

                break;
        }

        EventManager.instance.RunEvent("CardClickedPlayer");

        string cardName = container.card.name;

        if (cardName.EndsWith("(Clone)"))
        {
            cardName = cardName.Substring(0, cardName.Length - 7);
        }

        EventManager.instance.RunEvent("CardPlaced_" + cardName);
    }

    public void CardReturnPlayer(CardContainer container)
    {
        if (!playersTurn) return;
        if (container.empty) return;
        if (container.card.category == BaseCardClass.Category.GAME_LOGIC) return;

        List<Transform> emptySpaces = CheckEmptyPositions(playerDeckHand);
        if (emptySpaces.Count == 0) return;

        container.card.PlaceInHand(emptySpaces[0], false);

        UpdateNextButton(CheckEmptyPositions(playerPlayedCards).Count);

        EventManager.instance.RunEvent("CardClickedPlayedPlayer");
    }

    void UpdateNextButton(int emptySpaces)
    {
        nextTurnButton.gameObject.GetComponent<Image>().sprite = spritesNextButton[2 - emptySpaces];

        if (emptySpaces == 0)
        {
            nextTurnButton.interactable = true;
            EventManager.instance.RunEvent("NextButtonEnabled");
            if(playersTurn) GameObject.Find("Opponent").GetComponent<Joel_OpponentBarks>().ReplyBark();
        }
        else
        {
            nextTurnButton.interactable = false;
        }
    }

    void CardClickedEnemy(CardContainer container)
    {
        if (!enemyCanPlayCards) return;

        if (container.card.category == BaseCardClass.Category.STRATEGY && !enemyHasPlayedSpecialEffect)
        {
            switch (container.card.specialEffect)
            {
                case BaseCardClass.SpecialEffects.DRAW_NEW_HAND:
                case BaseCardClass.SpecialEffects.BRING_BACK_X_CARDS_FROM_DISCARDED:
                case BaseCardClass.SpecialEffects.SEARCH_TOP_X_IN_STACK_FOR_Y_CARDS:
                    container.card.Discard(enemyDiscardPile.transform);
                    FillDeck(enemyDeckHand, enemyDeck, false);
                    break;
                
                case BaseCardClass.SpecialEffects.LOCK_ONE_CARD_POSITION_BOTH:
                    container.card.Discard(enemyDiscardPile.transform);

                    isLockedPositions = 2;
                    UpdateLockedPositions();
                    break;
            }

            enemyHasPlayedSpecialEffect = true;
        }
        else
        {
            List<Transform> emptySpaces = CheckEmptyPositions(enemyPlayedCards);
            if (emptySpaces.Count == 0) return;

            container.card.MoveToBoard(emptySpaces[0]);
            FillDeck(enemyDeckHand, enemyDeck, false);
        }

        EventManager.instance.RunEvent("CardClickedEnemy");

        string cardName = container.card.name;

        if (cardName.EndsWith("(Clone)"))
        {
            cardName = cardName.Substring(0, cardName.Length - 7);
        }

        EventManager.instance.RunEvent("CardPlaced_" + cardName);
    }

    List<Transform> CheckEmptyPositions(GameObject inputDeck)
    {
        return inputDeck.transform.Cast<Transform>().Where(
            c => c.childCount == 0
        ).ToList();
    }

    void NextTurn()
    {
        EventManager.instance.RunEvent("ClickNextTurnButton");

        CalculateCardValues(playerPlayedCards, enemyPlayedCards);
        

        CheckEndGame();
        if (gameOver) return;
        
        nextTurnButton.interactable = false;

        if (isLockedPositions > 0) isLockedPositions -= 1;
        UpdateLockedPositions();

        // do final add of trust to support (not final, is first)
        int supportToBe = supportSliderValue - trustEnemy;
        StartCoroutine(UpdateSupportBarWithTrust(supportToBe, trustSliderEnemy));
        supportSliderValue = supportToBe;

        ClearPlayedCards(enemyPlayedCards);
        FillDeck(playerDeckHand, playerDeck, true);

        hasPlayedSpecialEffect = false;

        playersTurn = false;

        EnemyPlayCard();
    }

    void UpdateLockedPositions()
    {
        bool spritesCreated = stopSpritePlayer && stopSpriteEnemy;

        if (isLockedPositions > 0 && !spritesCreated)
        {
            // TODO: Make this work no matter which side it gets played on.
            CardContainer playerContainer = playerPlayedCards.transform.GetChild(1).GetComponent<CardContainer>();

            // Check if there's already a card on the spot we want to lock.
            if (!playerContainer.empty)
            {
                List<Transform> emptySpaces = CheckEmptyPositions(playerDeckHand);

                if (emptySpaces.Count > 0)
                {
                    playerContainer.card.PlaceInHand(emptySpaces[0], false);
                }
            }

            // We're assuming that there's no card to remove on the enemy's side of the board.
            stopSpritePlayer = Instantiate(stopSprite, playerContainer.transform);
            stopSpriteEnemy  = Instantiate(stopSprite, enemyPlayedCards.transform.GetChild(1));
        }
        else if (isLockedPositions == 0 && spritesCreated)
        {
            Destroy(stopSpritePlayer);
            Destroy(stopSpriteEnemy);
        }
    }

    void ClearPlayedCards(GameObject playedCardsInput)
    {
        foreach (CardContainer container in playedCardsInput.GetComponentsInChildren<CardContainer>())
        {
            if (container.empty) continue;

            if (playersTurn)
            {
                container.card.Discard(enemyDiscardPile.transform);
                EventManager.instance.RunEvent("ClearPlayedCardsPlayer");
            }
            else
            {
                container.card.Discard(playerDiscardPile.transform);
                EventManager.instance.RunEvent("ClearPlayedCardsEnemy");
            }
        }
    }

    void EnemyPlayCard()
    {
        if (true)
        {
            StartCoroutine(EnemyTurnPausePlay());
        }
    }

    IEnumerator EnemyTurnPausePlay()
    {
        yield return new WaitForSeconds(pauseTimeBeforeRound);
        enemyTurnThing.GetComponent<MoveBetweenNPointsBouncyboi>().StartMovement();
        EventManager.instance.RunEvent("EnemysTurn");
        yield return new WaitForSeconds(3.5f);

        //EnemyAI aiComponent = AI.GetComponent<EnemyAI>();

        JoelAI.instance.ChooseCards();
        //aiComponent.ChooseACard();

        if (true)
        {
            float numberOfCards = 2;

            for (int i = 0; i < numberOfCards; i++)
            {
                if (true)
                {
                    //Debug.Log(aiComponent.chosenCards[i].card);

                    CardClickedEnemy(
                        JoelAI.instance.chosenCards[i]

                    );
                    yield return new WaitForSeconds(enemyTurnTime);
                    EventManager.instance.RunEvent("EnemyPlaceCard");
                }
            }
        }

        StartCoroutine(EnemyTurnPauseNextRound());
    }

    IEnumerator EnemyTurnPauseNextRound()
    {
        yield return new WaitForSeconds(0.8f);

        CalculateCardValues(enemyPlayedCards, playerPlayedCards);
        CheckEndGame();
        if (gameOver) yield break;

        if (!gameOver)
        {
            int supportToBe = supportSliderValue + trustPlayer;
            StartCoroutine(UpdateSupportBarWithTrust(supportToBe, trustSliderPlayer));
            supportSliderValue = supportToBe;

            GameObject.Find("Opponent").GetComponent<Joel_OpponentBarks>().PlayCardBark();

            yield return new WaitForSeconds(pauseTimeBeforeRound);

            EventManager.instance.RunEvent("PlayersTurn");
            yourTurnThing.GetComponent<MoveBetweenNPointsBouncyboi>().StartMovement();

            yield return new WaitForSeconds(1f);
            if (isLockedPositions > 0) isLockedPositions -= 1;
            UpdateLockedPositions();
            yield return new WaitForSeconds(0.8f);
            // TODO: Check if this should wait until after animation is finished.
            nextTurnButton.interactable = true;
            hasPlayedSpecialEffect = false;

            ClearPlayedCards(playerPlayedCards);

            UpdateNextButton(CheckEmptyPositions(playerPlayedCards).Count);

            playersTurn = true;
        }
    }
        
    void CalculateCardValues(GameObject currentPlayedCards, GameObject opponentPlayedCards)
    {

        // Temporary values for support and trust.
        int trustPlayerToBe = trustPlayer;
        int trustEnemyToBe = trustEnemy;
        int supportSliderToBe = supportSliderValue;
        float timeRemainingToBe = timeRemaining;
        int currentChildIndex = 0;
        bool[] requirementsLocalArray = new bool[7];
        requirementsMet.Clear();
        

        foreach (Transform child in currentPlayedCards.transform)
        {
            if (child.childCount == 0)
            {
                currentChildIndex++;
                continue;
            }

            BaseCardClass card = child.GetComponent<CardContainer>().card;
            GameObject cardGameObject = card.gameObject;

            if (playersTurn)
            {
                trustPlayerToBe += card.trust;
                supportSliderToBe += card.support;
            }
            else
            {
                trustEnemyToBe += card.trust;
                // Reverse direction of support meter for enemy.
                supportSliderToBe -= card.support;
            }

            timeRemainingToBe -= card.cost;

            // Move to next card if there's no effects on the current one.
            if (!card.hasEffect)
            {
                currentChildIndex++;
                continue;
            }

            othersHaveRequiredCategory = false;
            requirementsAreMet = false;
            

            #region "Friendly CategoryCheck"
            if (card.isAffectedByFriendly && card.friendlyCategoryAffectedBy.Length > 0)
            {
                int oppositeCard = (currentChildIndex == 0) ? 1 : 0;
                CardContainer oppositeCardContainer = currentPlayedCards.transform
                    .GetChild(oppositeCard).GetComponent<CardContainer>();

                if (oppositeCardContainer.empty)
                {
                    currentChildIndex++;
                    continue;
                }

                // If only let's say 1, 2 then go through twice checking if other card == 1 || 2.
                foreach (BaseCardClass.Category a in card.friendlyCategoryAffectedBy)
                {
                    othersHaveRequiredCategory |= oppositeCardContainer.card.category == a;
                }
            }
            #endregion
            #region "Enemy CategoryCheck"
            else if (!card.isAffectedByFriendly && card.enemyCategoryAffectedBy.Length > 0)
            {
                foreach (Transform containerTransform in opponentPlayedCards.transform)
                {
                    CardContainer container = containerTransform.GetComponent<CardContainer>();

                    if (container.empty) continue;

                    foreach (BaseCardClass.Category a in card.enemyCategoryAffectedBy)
                    {
                        othersHaveRequiredCategory |= container.card.category == a;
                    }
                }
            }
            #endregion
            else {
                // There's no requirements for other categories.
                othersHaveRequiredCategory = true;
            }

            if (!othersHaveRequiredCategory)
            {
                currentChildIndex++;
                continue;
            }

            #region "Others Requirement Check"
            switch (card.requirements)
            {
                case BaseCardClass.Requirements.NONE:
                    requirementsAreMet = true;
                    break;
                
                case BaseCardClass.Requirements.TRUST_SELF_LOWER_THAN_LIMIT:
                    requirementsAreMet = playersTurn ?
                        (trustPlayer < card.trustSelfLowerThanLimit) :
                        (trustEnemy < card.trustSelfLowerThanLimit);
                    break;
                
                case BaseCardClass.Requirements.TRUST_SELF_LARGER_THAN_LIMIT:
                    requirementsAreMet = playersTurn ?
                        (trustPlayer > card.trustSelfLargerThanLimit) :
                        (trustEnemy > card.trustSelfLargerThanLimit);
                    break;

                case BaseCardClass.Requirements.TRUST_SELF_LOWER_THAN_ENEMY:
                    requirementsAreMet = playersTurn ?
                        (trustPlayer < trustEnemy) :
                        (trustEnemy < trustPlayer);
                    break;

                case BaseCardClass.Requirements.TRUST_SELF_LARGER_THAN_ENEMY:
                    requirementsAreMet = playersTurn ?
                        (trustPlayer > trustEnemy) :
                        (trustEnemy > trustPlayer);
                    break;
                
                case BaseCardClass.Requirements.SUPPORT_SELF_LOWER_THAN_LIMIT:
                    requirementsAreMet = playersTurn ?
                        (supportSliderValue < card.supportSelfLowerThanLimit) :
                        (supportSliderValue > card.supportSelfLowerThanLimit);
                    break;
                
                case BaseCardClass.Requirements.SUPPORT_SELF_LARGER_THAN_LIMIT:
                    requirementsAreMet = playersTurn ?
                        (supportSliderValue < card.supportSelfLargerThanLimit) :
                        (supportSliderValue > card.supportSelfLargerThanLimit);
                    break;
                
            }
            #endregion
            requirementsMet.Add(requirementsAreMet);
            requirementsLocalArray[currentChildIndex] = requirementsAreMet;

            if (!requirementsAreMet)
            {
                currentChildIndex++;
                continue;
            }

            #region "Effects Do If Requirements Are Met"
            switch (card.effects)
            {
                case BaseCardClass.Effects.ADD_SUPPORT_SELF:
                    if (playersTurn) { supportSliderToBe += card.supportOnEffect; }
                    else             { supportSliderToBe -= card.supportOnEffect; }
                    break;
                
                case BaseCardClass.Effects.ADD_TRUST_SELF:
                    if (playersTurn) { trustPlayerToBe += card.trustOnEffect; }
                    else             { trustEnemyToBe  += card.trustOnEffect; }
                    break;
                
                case BaseCardClass.Effects.ADD_TRUST_SUPPORT_SELF:
                    if (playersTurn)
                    {
                        trustPlayerToBe += card.trustOnEffect;
                        supportSliderToBe += card.supportOnEffect;
                    }
                    else
                    {
                        trustEnemyToBe += card.trustOnEffect;
                        supportSliderToBe -= card.supportOnEffect;
                    }
                    break;
                
                case BaseCardClass.Effects.ADD_TRUST_ENEMY:
                    if (playersTurn) { trustEnemyToBe  += card.trustOnEffect; }
                    else             { trustPlayerToBe += card.trustOnEffect; }
                    break;
                
                case BaseCardClass.Effects.REMOVE_TRUST_ENEMY:
                    if (playersTurn) { trustEnemyToBe  -= card.trustOnEffect; }
                    else             { trustPlayerToBe -= card.trustOnEffect; }
                    break;

                case BaseCardClass.Effects.ADD_SUPPORT_SELF_ADD_TRUST_ENEMY:
                    if (playersTurn)
                    {
                        supportSliderToBe += card.supportOnEffect;
                        trustEnemyToBe += card.trustOnEffect;
                    }
                    else
                    {
                        supportSliderToBe -= card.supportOnEffect;
                        trustPlayerToBe += card.trustOnEffect;
                    }
                    break;
                
                case BaseCardClass.Effects.GAIN_PERCENTAGE_OF_DIFFERENCE:
                    int enemySupport = (int)supportSlider.maxValue - supportSliderValue;
                    int difference = Mathf.Abs((int)supportSlider.maxValue - enemySupport);
                    int supportToAdd = difference * (card.percentOnEffect / 100);

                    if (playersTurn) { supportSliderToBe += supportToAdd; }
                    else             { supportSliderToBe -= supportToAdd; }
                    break;
                 
            }

            

            #endregion
        }

        for(int i = 0; i < 7; i++)
        {
            requirementsMet.Add(requirementsLocalArray[i]);
        }

        if (timeRemainingToBe < 0) timeRemainingToBe = 0;

        supportSliderToBe = Mathf.Clamp(supportSliderToBe, 0, (int)supportSlider.maxValue);
        trustPlayerToBe = Mathf.Clamp(trustPlayerToBe, 0, trustMax);
        trustEnemyToBe = Mathf.Clamp(trustEnemyToBe, 0, trustMax);

        //do trust event thingy, yes
        CheckHighestTrustChange(Mathf.Abs(trustPlayer - trustPlayerToBe), true);
        CheckHighestTrustChange(Mathf.Abs(trustEnemy - trustEnemyToBe), false);

        // Apply changes.
        StartCoroutine(UpdateTrustBar(trustPlayerToBe, true));
        StartCoroutine(UpdateTrustBar(trustEnemyToBe, false));

        //do support event thingy
        CheckHighestSupportChange(Mathf.Abs(supportSliderValue - supportSliderToBe), 
            supportSliderValue > supportSliderToBe);

        // Send to update with new updated support value.
        StartCoroutine(UpdateSupportBar(supportSliderToBe));

        //update Timer with new courutine
        StartCoroutine(UpdateTime(timeRemainingToBe));

        //set values so endcheck is neato
        trustPlayer = trustPlayerToBe;
        trustEnemy = trustEnemyToBe;
        supportSliderValue = supportSliderToBe;
        timeRemaining = timeRemainingToBe;
    }

    void CheckHighestSupportChange(int change, bool changeIsNegative)
    {
        int changedBy = 0;
        if (change <= 5) changedBy = 5;
        else if (change <= 10) changedBy = 10;
        else if (change <= 15) changedBy = 15;
        else if (change <= 20) changedBy = 20;
        else if (change <= 25) changedBy = 25;
        else if (change <= 50) changedBy = 50;

        if (changeIsNegative) EventManager.instance.RunEvent("SupportBarDecreaseBy" + changedBy);
        else EventManager.instance.RunEvent("SupportBarIncreaseBy" + changedBy);

        OpponentAnimationController opponentThingy = GameObject.Find("Opponent").GetComponent<OpponentAnimationController>();
        AudienceAnimatorController audienceThingy = GameObject.Find("Audience").GetComponent<AudienceAnimatorController>();

        if (changeIsNegative)
        {
            switch (changedBy)
            {
                case 0: break;
                case 5: opponentThingy.MakeSmugForXTime(1f); break;
                case 10: opponentThingy.MakeSmugForXTime(2f); break;
                case 15: opponentThingy.MakeSmugForXTime(2.5f);
                            audienceThingy.MakeAngryForXTime(4f); break;
                case 20: opponentThingy.MakeSmugForXTime(3f); break;
                case 25: opponentThingy.MakeSmugForXTime(3f); break;
                case 50: opponentThingy.MakeSmugForXTime(5f); break;
            }
        }
        else
        {
            switch (changedBy)
            {
                case 0: opponentThingy.MakeAnnoyedForXTime(1f); break;
                case 5: opponentThingy.MakeAnnoyedForXTime(2f); break;
                case 10: opponentThingy.MakeAnnoyedForXTime(3f); break;
                case 15: opponentThingy.MakeAnnoyedForXTime(3.5f);
                            audienceThingy.MakeCheerForXTime(4f); break;
                case 20: opponentThingy.MakeAnnoyedForXTime(4f);
                            audienceThingy.MakeCheerForXTime(4f); break;
                case 25: opponentThingy.MakeAngeryForXTime(3f);
                            audienceThingy.MakeCheerForXTime(4f); break;
                case 50: opponentThingy.MakeAngeryForXTime(5f);
                            audienceThingy.MakeCheerForXTime(4f); break;
            }
        }
        
    }

    void CheckHighestTrustChange(int change, bool isPlayer)
    {
        //>:D
        if (change >= 4)
        {
            if(isPlayer) EventManager.instance.RunEvent("TrustPlayerIncreaseBy" + 4);
            else EventManager.instance.RunEvent("TrustEnemyIncreaseBy" + 4);
        }
    }

    IEnumerator UpdateSupportBar(int supportSliderToBe)
    {
        isRunningFirstStageSupport = true;

        int supportSliderValueTemp = supportSliderValue;

        // Can be zero, negative or positive.
        int changeInSupportSlider = supportSliderToBe - supportSliderValueTemp;

        float pauseTime = 0.05f;
        pauseTimeBeforeRound = pauseTime * Mathf.Abs(changeInSupportSlider);
        pauseTimeBeforeRound += pauseTime * (playersTurn ? trustEnemy : trustPlayer) * 2;

        if (changeInSupportSlider == 0)
        {
            EventManager.instance.RunEvent("SupportBar_NoChange");
            isRunningFirstStageSupport = false;
            yield break;
        }
        else if (changeInSupportSlider > 0)
        {
            EventManager.instance.RunEvent("SupportBar_StartIncrease");

            for (int i = supportSliderValueTemp; i < supportSliderToBe; i++)
            {
                supportSliderValueTemp++;
                supportSlider.value = supportSliderValueTemp;
                yield return new WaitForSeconds(pauseTime);
                EventManager.instance.RunEvent("SupportBar_Increase");
            }

            EventManager.instance.RunEvent("SupportBar_EndIncrease");

            if (changeInSupportSlider >= 20)
            {
                EventManager.instance.RunEvent("SupportIncreaseExceedsX");
            }
        }
        else if (changeInSupportSlider < 0)
        {
            EventManager.instance.RunEvent("SupportBar_StartDecrease");

            for (int i = supportSliderValueTemp; i > supportSliderToBe; i--)
            {
                supportSliderValueTemp--;
                supportSlider.value = supportSliderValueTemp;
                yield return new WaitForSeconds(pauseTime);
                EventManager.instance.RunEvent("SupportBar_Decrease");
            }

            EventManager.instance.RunEvent("SupportBar_EndDecrease");
        }
        

        isRunningFirstStageSupport = false;
    }

    IEnumerator UpdateSupportBarWithTrust(int supportSliderToBe, GameObject trustBarFrom)
    {
        int currentChangePos = 0;
        int supportSliderValueTemp = supportSliderValue;
        int changeInSupportSlider = supportSliderToBe - supportSliderValueTemp;

        float pauseTime = 0.05f;

        yield return new WaitUntil(() => isRunningFirstStageSupport == false);
        if (playersTurn) yield return new WaitForSeconds(2.5f);
        else yield return new WaitForSeconds(2.5f);

        //can be 0, -, +
        if (changeInSupportSlider == 0)
        {
            yield break;
        }
        else if (changeInSupportSlider > 0)
        {
            EventManager.instance.RunEvent("SecondStarChangeSupportBar_StartIncrease");

            for (int i = supportSliderValueTemp; i < supportSliderToBe; i++)
            {
                supportSliderValueTemp++;
                supportSlider.value = supportSliderValueTemp;

                CreateShootingStar(
                    trustBarFrom.transform.GetChild(currentChangePos),
                    supportSlider.handleRect.transform
                );

                currentChangePos++;
                yield return new WaitForSeconds(pauseTime);
                EventManager.instance.RunEvent("SecondStarChangeSupportBar_Increase");
            }

            EventManager.instance.RunEvent("SecondStarChangeSupportBar_StopIncrease");
        }
        else if (changeInSupportSlider < 0)
        {
            EventManager.instance.RunEvent("SecondStarChangeSupportBar_StartDecrease");

            for (int i = supportSliderValueTemp; i > supportSliderToBe; i--)
            {
                supportSliderValueTemp--;
                supportSlider.value = supportSliderValueTemp;

                CreateShootingStar(
                    trustBarFrom.transform.GetChild(currentChangePos),
                    supportSlider.handleRect.transform
                );

                currentChangePos++;
                yield return new WaitForSeconds(pauseTime);
                EventManager.instance.RunEvent("SecondStarChangeSupportBar_Decrease");
            }

            EventManager.instance.RunEvent("SecondStarChangeSupportBar_StopDecrease");
        }
    }

    IEnumerator UpdateTrustBar(int trustToBe, bool isFriendly)
    {
        int trustPlayerTemp = trustPlayer;
        int trustEnemyTemp = trustEnemy;
        float waitForTime = 0.2f;

        //can be 0, -, +
        int changeInTrust = trustToBe - (isFriendly ? trustPlayerTemp : trustEnemyTemp);

        if (changeInTrust == 0) yield break;

        if (isFriendly)
        {
            if (changeInTrust > 0)
            {
                for (int i = trustPlayerTemp; i < trustToBe; i++)
                {
                    trustPlayerTemp++;
                    SetTrustOnSlider(trustSliderPlayer, trustPlayerTemp);
                    EventManager.instance.RunEvent("TrustPlayerIncrease");
                    yield return new WaitForSeconds(waitForTime);
                }
            }
            else if (changeInTrust < 0)
            {
                for (int i = trustPlayerTemp; i > trustToBe; i--)
                {
                    trustPlayerTemp--;
                    SetTrustOnSlider(trustSliderPlayer, trustPlayerTemp);
                    EventManager.instance.RunEvent("TrustPlayerDecrease");
                    yield return new WaitForSeconds(waitForTime);
                }
            }
        }
        else
        {
            if (changeInTrust > 0)
            {
                for (int i = trustEnemyTemp; i < trustToBe; i++)
                {
                    trustEnemyTemp++;
                    SetTrustOnSlider(trustSliderEnemy, trustEnemyTemp);
                    EventManager.instance.RunEvent("TrustEnemyIncrease");
                    yield return new WaitForSeconds(waitForTime);
                }
            }
            else if (changeInTrust < 0)
            {
                for (int i = trustEnemyTemp; i > trustToBe; i--)
                {
                    trustEnemyTemp--;
                    SetTrustOnSlider(trustSliderEnemy, trustEnemyTemp);
                    EventManager.instance.RunEvent("TrustEnemyDecrease");
                    yield return new WaitForSeconds(waitForTime);
                }
            }
        }
    }

    IEnumerator UpdateTime(float timeRemainingToBe)
    {
        float timeRemainingTemp = timeRemaining;

        for (float i = timeRemaining; i > timeRemainingToBe; i--)
        {
            timeRemainingTemp--;
            clockText.text = timeRemainingTemp + ":00";

            yield return new WaitForSeconds(0.08f);
            EventManager.instance.RunEvent("Time_Decrease");

            // Send event due to clock running low.
            if (timeRemainingTemp <= 10 && !hasPassedTen)
            {
                EventManager.instance.RunEvent("TimeLowerThan10");
                StartCoroutine(BlinkClockXTimesWithYDelay(3, 0.15f));
                hasPassedTen = true;
            }
        }
    }

    IEnumerator BlinkClockXTimesWithYDelay(int timesToBlink, float delayOnBlink)
    {
        for (int i = 0; i < timesToBlink; i++)
        {
            clockBlinker.SetActive(true);
            yield return new WaitForSeconds(delayOnBlink);
            clockBlinker.SetActive(false);
            yield return new WaitForSeconds(delayOnBlink);
        }
    }

    public void CreateShootingStar(Transform startPosition, Transform endPosition)
    {
        ShootingStarController shootingStarTemp = Instantiate(shootingStarPrefab).GetComponent<ShootingStarController>();
        shootingStarTemp.startPosition = startPosition;
        shootingStarTemp.targetPosition = endPosition;

        EventManager.instance.RunEvent("CreateShootingStar");
    }

    void DrawNewHand(GameObject inputDeckHand, GameObject inputPlayedCards, GameObject inputCardBackStack)
    {
        // first push all on this side into stack, then pull out stack into array, mix array, push back into stack, do filldeck
        // first take all from DeckHand push into stack.
        foreach (Transform child in inputDeckHand.transform)
        {
            CardContainer container = child.GetComponent<CardContainer>();
            if (container.empty) continue;

            container.card.PlaceInDeck(inputCardBackStack.transform);
        }

        // Then played cards.
        foreach (Transform child in inputPlayedCards.transform)
        {
            CardContainer container = child.GetComponent<CardContainer>();
            if (container.empty) continue;

            container.card.PlaceInDeck(inputCardBackStack.transform);
        }

        ShuffleStack(inputCardBackStack.transform, 50);

        // Now fill hand deck back.
        FillDeck(inputDeckHand, inputCardBackStack, playersTurn);
        EventManager.instance.RunEvent("DrawNewHandUse");
    }

    void ShuffleStack(Transform stack, int iterations, int maxSize = -1)
    {
        int count = (maxSize < 0) ?
            stack.childCount :
            Mathf.Min(stack.childCount, maxSize);

        for (int i = 0; i < iterations; i++)
        {
            // Take random card in child, put to dealer hand then set back into top of tree as child.
            int cardIndex = Random.Range(1, count);
            GameObject card1 = stack.GetChild(cardIndex).gameObject;
            card1.transform.SetParent(dealerHands.transform);
            card1.transform.SetParent(stack);
        }
    }

    IEnumerator DisplayInspector(BaseCardClass inputCard, GameObject stack, GameObject hand, string openEvent, string closeEvent)
    {
        SetBoardInteractable(false);
        pauseButton.SetActive(false);
        helpButton.SetActive(false);

        inspectorDeck.SetActive(true);

        overlay.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.4f);

        inspectorText.SetActive(true);

        int cardsToShow = Mathf.Min(
            inputCard.cardsTopStackViewCount,
            stack.transform.childCount
        );

        for (int i = 0; i < cardsToShow; i++)
        {
            BaseCardClass card = stack.transform.GetChild(0).GetComponent<BaseCardClass>();
            card.ShowInInspector(inspectorDeck.transform.GetChild(i));
            inspectorDeck.transform.GetChild(i).position = new Vector3(
                inspectorDeck.transform.GetChild(i).position.x,
                inspectorDeck.transform.GetChild(i).position.y, -5
            );
        }

        EventManager.instance.RunEvent(openEvent);

        // Cards are selected on first click, two clicks on same card picks it.
        yield return new WaitUntil(() => hasChosenCard);

        EventManager.instance.RunEvent(closeEvent);

        foreach (Transform child in inspectorDeck.transform)
        {
            CardContainer container = child.GetComponent<CardContainer>();

            if (container.empty) continue;

            child.position = new Vector3(child.position.x, child.position.y, 0);

            if (container.GetInstanceID() == inspectorSelectedCard)
            {
                List<Transform> emptySpaces = CheckEmptyPositions(hand);
                if (emptySpaces.Count == 0) continue;

                container.card.PlaceInHand(emptySpaces[0], false);
            }
            else {
                container.card.gameObject.SetActive(false);
                container.card.transform.SetParent(stack.transform);
            }
        }

        ShuffleStack(stack.transform, 50);

        overlay.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);

        inspectorText.SetActive(false);

        inspectorDeck.SetActive(false);

        SetBoardInteractable(true);
        pauseButton.SetActive(true);
        helpButton.SetActive(true);

        hasChosenCard = false;
        inspectorSelectedCard = 0;
    }

    public void SelectCardInspector(CardContainer container)
    {
        if (container.empty) return;
        
        inspectorSelectedCard = container.GetInstanceID();
        hasChosenCard = true;
    }

    public void SetBoardInteractable(bool interactable)
    {
        boardInteractable = interactable;

        nextTurnButton.GetComponent<Button>().enabled = interactable;
        

        //Andr, plez fix colour
        if (pauseMenuIsOpen)
        {
            helpButton.GetComponent<Image>().color = new Color(155, 155, 155);
            helpButton.GetComponent<Button>().enabled = false;
        }
        else if (helpMenuIsOpen)
        {
            pauseButton.GetComponent<Image>().color = new Color(150, 150, 150);
            pauseButton.GetComponent<Button>().enabled = false;
        }
        else if (groupImageIsOpen)
        {
            helpButton.GetComponent<Image>().color = new Color(150, 150, 150);
            helpButton.GetComponent<Button>().enabled = false;
            pauseButton.GetComponent<Image>().color = new Color(150, 150, 150);
            pauseButton.GetComponent<Button>().enabled = false;
        }
        else
        {
            pauseButton.GetComponent<Button>().enabled = true;
            helpButton.GetComponent<Button>().enabled = true;
            groupImage.GetComponent<Button>().enabled = true;
        }
    }

    public static List<GameObject> ShuffleList(List<GameObject> list)
    {
        return list.OrderBy(x => Random.value).ToList();;
    }

    void CheckEndGame()
    {
        // If support is 0 or 100.
        if (supportSliderValue >= 100)
        {
            EventManager.instance.RunEvent("KnockoutWinByPlayer");
            gameOver = true;
            StartCoroutine(EndGame(true));
        }
        else if (supportSliderValue <= 0)
        {
            EventManager.instance.RunEvent("KnockoutWinByEnemy");
            gameOver = true;
            StartCoroutine(EndGame(false));
        }

        // If time has ran out.
        if (timeRemaining <= 0)
        {
            EventManager.instance.RunEvent("OutOfTime");

            if (supportSliderValue == 50)
            {
                EventManager.instance.RunEvent("OutOfTime_Tie");
                gameOver = true;
                StartCoroutine(EndGame(true));
            }
            else if (supportSliderValue > 50)
            {
                EventManager.instance.RunEvent("OutOfTime_PlayerWin");
                gameOver = true;
                StartCoroutine(EndGame(true));
            }
            else if (supportSliderValue < 50)
            {
                EventManager.instance.RunEvent("OutOfTime_EnemyWin");
                gameOver = true;
                StartCoroutine(EndGame(false));
            }
        }
    }

    IEnumerator EndGame(bool playerWon)
    {
        SetBoardInteractable(false);
        nextTurnButton.interactable = false;

        yield return new WaitForSeconds(0.5f);

        if(playerWon)
        {
            GameObject.Find("Opponent").GetComponent<OpponentAnimationController>().MakeAngeryForXTime(10f);
            YouWin_First.GetComponent<MoveFromXtoYviaDirectionBouncyboi>().StartMovement();
            YouWin_Second.GetComponent<MoveFromXtoYviaDirectionBouncyboi>().StartMovement();
        }
        else
        {
            GameObject.Find("Opponent").GetComponent<OpponentAnimationController>().MakeSmugForXTime(10f);
            YouLose_First.GetComponent<MoveFromXtoYviaDirectionBouncyboi>().StartMovement();
            YouLose_Second.GetComponent<MoveFromXtoYviaDirectionBouncyboi>().StartMovement();
        }

        yield return new WaitForSeconds(6f);

        // Zoom in on thing based on who won.
        if (playerWon)
        {
            EventManager.instance.RunEvent("Goto_Board1Victory");
            GameManager.instance.GoToScene(sceneOnWin);
        }
        else
        {
            EventManager.instance.RunEvent("Goto_Board1GameOver");
            GameManager.instance.GoToScene(sceneOnLose);
        }
    }

    public void TogglePauseMenu()
    {
        pauseMenuIsOpen = !pauseMenuIsOpen;
        pauseMenu.SetActive(pauseMenuIsOpen);

        SetBoardInteractable(!pauseMenuIsOpen);
        overlay.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, pauseMenuIsOpen ? 0.4f : 0);
    }

    public void ToggleHelpMenu()
    {
        helpMenuIsOpen = !helpMenuIsOpen;
        helpMenu.SetActive(helpMenuIsOpen);

        SetBoardInteractable(!helpMenuIsOpen);
        overlay.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, helpMenuIsOpen ? 0.4f : 0);
    }

    public void ToggleGroupImage()
    {
        groupImageIsOpen = !groupImageIsOpen;
        groupImage.SetActive(groupImageIsOpen);

        SetBoardInteractable(!groupImageIsOpen);
        overlay.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, groupImageIsOpen ? 0.4f : 0);
    }
}
