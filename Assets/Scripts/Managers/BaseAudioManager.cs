﻿using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class BaseAudioManager : MonoBehaviour
{
    [Header("Audio source pool settings")]
    public GameObject audioPlayerPrefab;
    public int sourcePoolSize;

    public virtual bool  Muted  { get; set; }
    public virtual float Volume { get; set; }

    protected AudioSource source;

    protected Queue<GameObject> sourceQueue = new Queue<GameObject>();

    protected void Initialize()
    {
        for (int i = 0; i < sourcePoolSize; i++)
        {
            GameObject audioPlayer = Instantiate(audioPlayerPrefab, transform);
            audioPlayer.SetActive(false);

            sourceQueue.Enqueue(audioPlayer);
        }
    }

    protected GameObject GetNewSource()
    {
        if (sourceQueue.Count == 0)
        {
            return Instantiate(audioPlayerPrefab, transform);
        }
        else
        {
            GameObject newSource = sourceQueue.Dequeue();
            newSource.SetActive(true);

            return newSource;
        }
    }

    protected void ReturnSource(GameObject obj)
    {
        obj.SetActive(false);
        sourceQueue.Enqueue(obj);
    }

    protected void ConfigureSource(AudioSource src, AudioFile file)
    {
        src.clip = file.file;
        src.loop = file.loop;
        src.pitch = file.pitch;
        src.volume = Muted ? 0.0f : file.volume * Volume;
        src.priority = file.priority;
        src.outputAudioMixerGroup = file.mixer;

        SetFilterSettings<AudioDistortionFilter>(src.gameObject, file.distortionFilterSettings);
        SetFilterSettings<AudioEchoFilter>(src.gameObject, file.echoFilterSettings);
        SetFilterSettings<AudioHighPassFilter>(src.gameObject, file.highPassFilterSettings);
        SetFilterSettings<AudioLowPassFilter>(src.gameObject, file.lowPassFilterSettings);
        SetFilterSettings<AudioReverbFilter>(src.gameObject, file.reverbFilterSettings);
    }

    public virtual void Play(AudioFile file)
    {
        GameObject audioPlayer = GetNewSource();
        source = audioPlayer.GetComponent<AudioSource>();

        ConfigureSource(source, file);
        source.Play();

        if (!source.loop)
        {
            StartCoroutine(EnqueueWhenFinished(source));
        }
    }

    IEnumerator EnqueueWhenFinished(AudioSource audioPlayer)
    {
        yield return new WaitUntil(() => !audioPlayer.isPlaying);
        ReturnSource(audioPlayer.gameObject);
    }

    public void SetVolume(Slider slider)
    {
        // Take into account file volume.
        float prevFileVolume = source.volume / Volume;

        Volume = slider.value;
        source.volume = prevFileVolume * Volume;
    }

    public void ToggleMute()
    {
        Muted = !Muted;
        source.mute = !source.mute;
    }

    // TODO: Check if this is too expensive, and if so switch to manual assigning instead.
    protected void SetFilterSettings<T>(GameObject obj, FilterSettings settings)
        where T : Behaviour
    {
        T filter = obj.GetComponent<T>();
        filter.enabled = settings.overrideDefault;

        // Check if these settings should be applied.
        if (!settings.overrideDefault)
        {
            return;
        }

        System.Type filterType = filter.GetType();

        foreach (FieldInfo setting in settings.GetType().GetFields())
        {
            // Ignore the default override in the settings class.
            if (setting.Name == "overrideDefault") continue;

            filterType.GetProperty(setting.Name).SetValue(
                filter, setting.GetValue(settings), null
            );
        }
    }
}
