﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;
    public bool firstHelp = true;

    public static Texture2D LoadPNG(string filePath)
    {
        Texture2D tex = null;
        byte[] fileData;


        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }

        return tex;

    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        Cursor.SetCursor(LoadPNG("Assets/Sprites/UI sprites/CursorImage.png"), Vector2.zero, CursorMode.Auto);

        firstHelp = true;

    }

    public void GoToScene(string sceneName)
    {
        GameObject goToScene = Instantiate(Resources.Load("GoToScene", typeof(GameObject))) as GameObject;

        goToScene.GetComponent<GoToScene>().LoadScene(sceneName);
    }


    public void ExitGame()
    {
        EventManager.instance.RunEvent("ExitGame");
        Application.Quit();
    }

    public void RestartGame()
    {
        EventManager.instance.RunEvent("RestartGame");
        GoToScene("MainMenu");
        Destroy(gameObject);
    }

}
