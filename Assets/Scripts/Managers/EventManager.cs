﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class GameEvent
{
    public string name;
    public UnityEvent gameEvent;
}

public class EventManager : MonoBehaviour
{
    static public EventManager instance = null;

    public List<GameEvent> events = new List<GameEvent>();

    public bool logMissingEvents = true;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public void RunEvent(string eventName)
    {
        foreach (GameEvent e in events)
        {
            if (e.name == eventName)
            {
                e.gameEvent.Invoke();
                return;
            }
        }

        if (logMissingEvents)
        {
            Debug.Log("[INFO][EVENT] No event with the name " + eventName + " found.");
        }
    }
}
