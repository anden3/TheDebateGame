﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Joel_OpponentBarks : MonoBehaviour {
    [Header("grr Reply barks")]
    public string[] replyBark_RL;
    public string[] replyBark_KL;
    public string[] replyBark_RP;
    public string[] replyBark_KP;
    [Header("Angery play card barks")]
    public string[] playBark_RL;
    public string[] playBark_KL;
    public string[] playBark_RP;
    public string[] playBark_KP;
    public float timeWaitPerCharacterInString = 0.04f;
    public float timeWaitAfterAllWriteded = 2f;
    public float opacityPerFrame = 0.12f;
    [Space]
    public GameObject speachBubble;

    private GameObject playerPlayedCards;
    private GameObject enemyPlayedCards;

    private bool isPlaying = false;

    // Use this for initialization
    void Start () {
        Debug.Assert(speachBubble != null);
        Debug.Assert(speachBubble.transform.GetChild(0).GetComponent<Text>());
        speachBubble.SetActive(false);
        isPlaying = false;

        playerPlayedCards = GameObject.Find("PlayerPlayedCards");
        enemyPlayedCards = GameObject.Find("EnemyPlayedCards");
    }

    //enum dont work grr
    public void ReplyBark()
    {
        if (isPlaying) return;
        CardContainer currentCardContainer = playerPlayedCards.transform.GetChild(1).GetComponent<CardContainer>();
        Debug.Assert(currentCardContainer.empty == false);

        string stringToSend = "";
        
        switch (currentCardContainer.card.category)
        {
            case BaseCardClass.Category.PURE_LOGOS: stringToSend = replyBark_RL[Random.Range(0, replyBark_RL.Length)]; break;
            case BaseCardClass.Category.CORRUPT_LOGOS: stringToSend = replyBark_KL[Random.Range(0, replyBark_KL.Length)]; break;
            case BaseCardClass.Category.PURE_PATHOS: stringToSend = replyBark_RP[Random.Range(0, replyBark_RP.Length)]; break;
            case BaseCardClass.Category.CORRUPT_PATHOS: stringToSend = replyBark_KP[Random.Range(0, replyBark_KP.Length)]; break;
        }

        if (stringToSend != null || stringToSend != "")
        {
            StartCoroutine(DoWholeAnimationThing(stringToSend));
        }
    }
    public void PlayCardBark()
    {
        if (isPlaying) return;
        CardContainer currentCardContainer = enemyPlayedCards.transform.GetChild(1).GetComponent<CardContainer>();
        Debug.Assert(currentCardContainer.empty == false);

        string stringToSend = "";

        switch (currentCardContainer.card.category)
        {
            case BaseCardClass.Category.PURE_LOGOS: stringToSend = playBark_RL[Random.Range(0, playBark_RL.Length)]; break;
            case BaseCardClass.Category.CORRUPT_LOGOS: stringToSend = playBark_KL[Random.Range(0, playBark_KL.Length)]; break;
            case BaseCardClass.Category.PURE_PATHOS: stringToSend = playBark_RP[Random.Range(0, playBark_RP.Length)]; break;
            case BaseCardClass.Category.CORRUPT_PATHOS: stringToSend = playBark_KP[Random.Range(0, playBark_KP.Length)]; break;
        }

        if (stringToSend != null || stringToSend != "")
        {
            StartCoroutine(DoWholeAnimationThing(stringToSend));
        }
    }

    IEnumerator DoWholeAnimationThing(string lineToWrite)
    {
        isPlaying = true;
        //Pop speachbubble in
        speachBubble.SetActive(true);
        //set text color at black
        speachBubble.transform.GetChild(0).GetComponent<Text>().color = new Color(0.19f, 0.19f, 0.19f, 1);

        //now fade lel xd
        speachBubble.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        for (float i = 0; i < 1; i += opacityPerFrame)
        {
            speachBubble.GetComponent<Image>().color = new Color(1, 1, 1, i);
            yield return new WaitForEndOfFrame();
        }
        speachBubble.GetComponent<Image>().color = new Color(1, 1, 1, 1);

        //WriteText
        for (int i = 0; i < lineToWrite.Length; i++)
        {
            speachBubble.transform.GetChild(0).GetComponent<Text>().text += lineToWrite[i];
            yield return new WaitForSeconds(timeWaitPerCharacterInString);
        }
        //Wait with it all out
        yield return new WaitForSeconds(timeWaitAfterAllWriteded);
        //Pop speachbubble out
        for (float i = 1; i > 0; i -= opacityPerFrame)
        {
            speachBubble.GetComponent<Image>().color = new Color(1, 1, 1, i);
            speachBubble.transform.GetChild(0).GetComponent<Text>().color = new Color(0.19f, 0.19f, 0.19f, i);
            yield return new WaitForEndOfFrame();
        }
        speachBubble.GetComponent<Image>().color = new Color(1, 1, 1, 0);

        speachBubble.SetActive(false);
        speachBubble.transform.GetChild(0).GetComponent<Text>().text = "";
        isPlaying = false;
    }
}
