﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckbuilderTutorialManager : MonoBehaviour {
    public GameObject[] frames;
    public GameObject okBtn;
    public GameObject letsChooseBtn;
    [Space]
    public GameObject scrollBar1;
    public GameObject scrollBar2;

    private int currentFrame = 0;
    private bool okBtnPressed = false;

	// Use this for initialization
	void Start () {
        if (GameManager.instance.firstHelp)
        {
            StartCoroutine(TutorialRoute());
        }
        else
        {
            transform.GetComponentInParent<Canvas>().sortingOrder = 0;

            foreach (GameObject frames in frames) frames.SetActive(false);
            DeckSelectorManager.instance.SetInteractable(true);
            okBtn.SetActive(false);
            letsChooseBtn.SetActive(false);
        }
    }

    IEnumerator TutorialRoute()
    {
        Debug.Log("start");
        yield return new WaitForEndOfFrame();
        DeckSelectorManager.instance.SetInteractable(false);
        okBtnPressed = false;
        scrollBar1.GetComponent<ScrollRect>().enabled = false;
        scrollBar1.GetComponent<ScrollRect>().StopMovement();
        scrollBar2.GetComponent<ScrollRect>().enabled = false;
        scrollBar2.GetComponent<ScrollRect>().StopMovement();
        okBtn.SetActive(true);
        letsChooseBtn.SetActive(false);

        for (int i = 0; i < frames.Length; i++)
        {
            Debug.Log("Loop");
            for (int k = 0; k < frames.Length; k++)
            {
                if (k == currentFrame) frames[k].SetActive(true);
                else frames[k].SetActive(false);
            }
            yield return new WaitForSeconds(0.2f);
            yield return new WaitUntil(() => okBtnPressed == true);
            okBtnPressed = false;
            currentFrame++;
            if (currentFrame == frames.Length - 1)
            {
                okBtn.SetActive(false);
                letsChooseBtn.SetActive(true);
            }
        }

        Debug.Log("end");
        DeckSelectorManager.instance.SetInteractable(true);
        okBtn.SetActive(false);
        letsChooseBtn.SetActive(false);
        scrollBar1.GetComponent<ScrollRect>().enabled = true;
        scrollBar2.GetComponent<ScrollRect>().enabled = true;

        transform.GetComponentInParent<Canvas>().sortingOrder = 0;

        foreach (GameObject frames in frames) frames.SetActive(false);
    }

    public void OkBtnClicked()
    {
        Debug.Log("Click");
        okBtnPressed = true;
    }
}
