﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : BaseAudioManager
{
    public static SoundManager instance = null;

    public static bool soundMuted = false;
    public static float soundVolume = 0.5f;

    public override bool Muted
    {
        get { return SoundManager.soundMuted; }
        set { SoundManager.soundMuted = value; }
    }

    public override float Volume
    {
        get { return SoundManager.soundVolume; }
        set { SoundManager.soundVolume = value; }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
        base.Initialize();
    }

    public override void Play(AudioFile file)
    {
        if (this != instance)
        {
            instance.Play(file);
            return;
        }

        base.Play(file);
    }
}
