﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicManager : BaseAudioManager
{
    public static MusicManager instance = null;

    public static bool musicMuted = false;
    public static float musicVolume = 0.5f;

    [Header("Fade settings")]
    public float fadeDuration = 2.0f;

    public override bool Muted
    {
        get { return MusicManager.musicMuted; }
        set { MusicManager.musicMuted = value; }
    }

    public override float Volume
    {
        get { return MusicManager.musicVolume; }
        set { MusicManager.musicVolume = value; }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
        base.Initialize();
    }

    public override void Play(AudioFile file)
    {
        if (this != instance)
        {
            instance.Play(file);
            return;
        }

        if (source == null || !source.isActiveAndEnabled || !source.isPlaying)
        {
            base.Play(file);
            return;
        }

        source.Stop();

        ConfigureSource(source, file);
        source.Play();
    }

    public void Stop()
    {
        if (this != instance)
        {
            instance.Stop();
            return;
        }

        source.Stop();
    }

    public void FadeOut()
    {
        if (source.isPlaying)
        {
            StartCoroutine(FadeOutCoroutine());
        }
    }

    public void FadeIn()
    {
        if (!source.isPlaying && source.clip != null)
        {
            StartCoroutine(FadeInCoroutine());
        }
    }

    // TODO: This only works once.
    public void CrossFade(AudioFile newMusic)
    {
        GameObject newPlayer = GetNewSource();
        AudioSource newSource = newPlayer.GetComponent<AudioSource>();

        Debug.Assert(source.GetInstanceID() != newSource.GetInstanceID());

        ConfigureSource(newSource, newMusic);
        StartCoroutine(CrossFadeCoroutine(newSource));
    }

    IEnumerator FadeOutCoroutine()
    {
        float startVolume = source.volume;

        while (source.volume > 0)
        {
            source.volume -= startVolume * Time.deltaTime / fadeDuration;
            yield return null;
        }

        source.Stop();
        source.volume = startVolume;
    }

    IEnumerator FadeInCoroutine()
    {
        float goal = source.volume;
        source.volume = 0;

        if (!source.isPlaying) source.Play();

        while (source.volume < goal)
        {
            source.volume += goal * Time.deltaTime / fadeDuration;
            yield return null;
        }
    }

    IEnumerator CrossFadeCoroutine(AudioSource newSource)
    {
        yield return StartCoroutine(FadeOutCoroutine());

        ReturnSource(source.gameObject);
        source = newSource;

        yield return StartCoroutine(FadeInCoroutine());
    }
}
