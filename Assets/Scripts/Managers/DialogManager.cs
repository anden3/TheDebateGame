﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialog
{
    public BaseCardClass card;
    public bool isSpecial;
    public AudioFile voiceOver;
    public string text;
}

public class DialogManager : MonoBehaviour
{
    public List<Dialog> dialog;
}
