﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudienceAnimatorController : MonoBehaviour {
    public Sprite sprIdle;
    public Sprite sprAngry;
    public Sprite sprCheer;
    SpriteRenderer sr;

    private void Start()
    {
        sr = gameObject.GetComponent<SpriteRenderer>();

        Debug.Log(sprIdle.name);
    }

    public void MakeIdleForXTime(float timeToBeFor)
    {
        sr.sprite = sprIdle;
        StartCoroutine(SetToDefaultAfterTime(timeToBeFor));
    }
    public void MakeAngryForXTime(float timeToBeFor)
    {
        sr.sprite = sprAngry;
        StartCoroutine(SetToDefaultAfterTime(timeToBeFor));
    }
    public void MakeCheerForXTime(float timeToBeFor)
    {
        sr.sprite = sprCheer;
        StartCoroutine(SetToDefaultAfterTime(timeToBeFor));
    }


    IEnumerator SetToDefaultAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        sr.sprite = sprIdle;
    }

}
