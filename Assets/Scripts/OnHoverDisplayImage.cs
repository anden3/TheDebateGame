﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnHoverDisplayImage : MonoBehaviour {

    public GameObject imageToShow;
    private bool state = false;
    private void Start()
    {
        imageToShow.SetActive(state);
    }
    private void Update()
    {
        imageToShow.SetActive(state);
    }
    private void OnMouseEnter()
    {
        state = true;
    }
    private void OnMouseExit()
    {
        state = false;
    }
}
