﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This is actually attached to a prefab which receives the events,
// so they will need to be propagated to the actual instance.

// TODO: Figure out a way to make this work better.

public class OpponentAnimationController : MonoBehaviour
{
    //Idle, Annoyed, Angery, Smug

    public void MakeIdleForXTime(float timeToBeFor)
    {
        //OpponentAnimationController instance = GameObject.Find("Opponent").GetComponent<OpponentAnimationController>();

        SetAnimatorState("IsIdle");
        StopAllCoroutines();
        StartCoroutine(SetToDefaultAfterTime(timeToBeFor));
    }
    public void MakeAnnoyedForXTime(float timeToBeFor)
    {
        //OpponentAnimationController instance = GameObject.Find("Opponent").GetComponent<OpponentAnimationController>();

        SetAnimatorState("IsAnnoyed");
        StopAllCoroutines();
        StartCoroutine(SetToDefaultAfterTime(timeToBeFor));
    }
    public void MakeAngeryForXTime(float timeToBeFor)
    {
        //OpponentAnimationController instance = GameObject.Find("Opponent").GetComponent<OpponentAnimationController>();

        SetAnimatorState("IsAngery");
        StopAllCoroutines();
        StartCoroutine(SetToDefaultAfterTime(timeToBeFor));
    }
    public void MakeSmugForXTime(float timeToBeFor)
    {
        //OpponentAnimationController instance = GameObject.Find("Opponent").GetComponent<OpponentAnimationController>();

        SetAnimatorState("IsSmug");
        StopAllCoroutines();
        StartCoroutine(SetToDefaultAfterTime(timeToBeFor));
    }

    private void SetAnimatorState(string whatToBe)
    {
        //Animator animator = GameObject.Find("Opponent_Mom").GetComponent<Animator>();

        string[] vars = {
            "IsIdle", "IsAnnoyed", "IsSmug", "IsAngery" 
        };

        foreach (string s in vars)
        {
            GetComponent<Animator>().SetBool(s, s == whatToBe);
        }
    }

    IEnumerator SetToDefaultAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        if(BoardManager.instance.supportSliderValue >= 75)
        {
            SetAnimatorState("IsAnnoyed");
        }
        else if(BoardManager.instance.supportSliderValue <= 25)
        {
            SetAnimatorState("IsSmug");
        }
        else
        {
            SetAnimatorState("IsIdle");
        }
    }
}
