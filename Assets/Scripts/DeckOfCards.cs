﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckOfCards : MonoBehaviour {
    [Header("Cards for deck in BoardManager")]
    public GameObject[] arrayOfCards;
}
