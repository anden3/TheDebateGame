﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(EventManager))]
public class EventManagerWindow : Editor
{
    private ReorderableList list;
    private SerializedProperty listProp;

    private SerializedProperty selectedEvent = null;

    void OnEnable()
    {
        list = new ReorderableList(
            serializedObject, serializedObject.FindProperty("events"),
            true, true, true, true
        );
        listProp = list.serializedProperty;

        list.drawHeaderCallback = rect => EditorGUI.LabelField(rect, "Events");

        list.drawElementCallback = (rect, index, isActive, isFocused) =>
        {
            SerializedProperty e = listProp.GetArrayElementAtIndex(index);
            rect.y += 2;

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, rect.width - 50, EditorGUIUtility.singleLineHeight),
                e.FindPropertyRelative("name"), GUIContent.none
            );
        };

        list.onSelectCallback = lst =>
        {
            selectedEvent = listProp.GetArrayElementAtIndex(lst.index);
        };

        list.onAddCallback = lst =>
        {
            // Expand array and select new element.
            int index = listProp.arraySize;
            listProp.arraySize++;
            lst.index = index;
            selectedEvent = listProp.GetArrayElementAtIndex(lst.index);

            SerializedProperty e = listProp.GetArrayElementAtIndex(index);
            e.FindPropertyRelative("name").stringValue = "";
            ClearUnityEvent(e.FindPropertyRelative("gameEvent"));
        };

        list.onRemoveCallback = lst =>
        {
            if (EditorUtility.DisplayDialog("Warning!", "Are you sure you want to delete the event?", "Yes", "No"))
            {
                ReorderableList.defaultBehaviours.DoRemoveButton(lst);

                if (listProp.arraySize > 0)
                {
                    selectedEvent = listProp.GetArrayElementAtIndex(lst.index);
                }
                else
                {
                    selectedEvent = null;
                }
            }
        };
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(
            serializedObject.FindProperty("logMissingEvents"), false
        );

        list.DoLayoutList();

        if (selectedEvent != null)
        {
            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(
                selectedEvent.FindPropertyRelative("gameEvent"), true
            );
        }

        serializedObject.ApplyModifiedProperties();
    }

    void ClearUnityEvent(SerializedProperty prop)
    {
        SerializedProperty calls = prop.FindPropertyRelative("m_PersistentCalls.m_Calls");

        for (int i = 0; i < calls.arraySize; i++)
        {
            calls.DeleteArrayElementAtIndex(i);
        }
    }
}