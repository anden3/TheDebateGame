﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(DialogManager))]
public class DialogManagerEditor : Editor
{
    private ReorderableList list;
    private SerializedProperty listProp;

    private SerializedProperty selectedDialog;

    void OnEnable()
    {
        list = new ReorderableList(
            serializedObject, serializedObject.FindProperty("dialog"),
            true, true, true, true
        );
        listProp = list.serializedProperty;

        list.drawHeaderCallback = (Rect rect) => EditorGUI.LabelField(rect, "Dialogue");

        list.drawElementCallback = (rect, index, isActive, isFocused) =>
        {
            SerializedProperty e = listProp.GetArrayElementAtIndex(index);
            rect.y += 2;

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, rect.width - 50, EditorGUIUtility.singleLineHeight),
                e.FindPropertyRelative("card"), GUIContent.none
            );
        };

        list.onSelectCallback = lst =>
        {
            selectedDialog = listProp.GetArrayElementAtIndex(lst.index);
        };

        list.onAddCallback = lst =>
        {
            // Expand array and select new element.
            int index = listProp.arraySize;
            listProp.arraySize++;
            lst.index = index;
            selectedDialog = listProp.GetArrayElementAtIndex(lst.index);
        };

        list.onRemoveCallback = lst =>
        {
            if (EditorUtility.DisplayDialog("Warning!", "Are you sure you want to delete the dialog?", "Yes", "No"))
            {
                ReorderableList.defaultBehaviours.DoRemoveButton(lst);

                if (listProp.arraySize > 0)
                {
                    selectedDialog = listProp.GetArrayElementAtIndex(lst.index);
                }
                else
                {
                    selectedDialog = null;
                }
            }
        };
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        list.DoLayoutList();

        if (selectedDialog != null)
        {
            EditorGUILayout.Space();
            
            EditorGUILayout.PropertyField(
                selectedDialog.FindPropertyRelative("isSpecial")
            );

            EditorGUILayout.PropertyField(
                selectedDialog.FindPropertyRelative("voiceOver")
            );

            EditorGUILayout.PropertyField(
                selectedDialog.FindPropertyRelative("text")
            );
        }

        serializedObject.ApplyModifiedProperties();
    }
}
