﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(BaseCardClass))]
public class BaseCardClassEditor : Editor
{
    private BaseCardClass myTarget;

    private ReorderableList friendlyList;
    private ReorderableList enemyList;

    private SerializedProperty friendlyListProp;
    private SerializedProperty enemyListProp;

    private bool displayEffects = false;

    void OnEnable()
    {
        myTarget = (BaseCardClass)target;

        friendlyList = new ReorderableList(
            serializedObject, serializedObject.FindProperty("friendlyCategoryAffectedBy"),
            false, true, true, true
        );

        enemyList = new ReorderableList(
            serializedObject, serializedObject.FindProperty("enemyCategoryAffectedBy"),
            false, true, true, true
        );

        friendlyListProp = friendlyList.serializedProperty;
        enemyListProp = enemyList.serializedProperty;

        InitializeReorderableList(friendlyList, friendlyListProp, "Affected by friendly categories:");
        InitializeReorderableList(enemyList, enemyListProp, "Affected by enemy categories:");
    }

    void InitializeReorderableList(ReorderableList list, SerializedProperty listProperty, string header)
    {
        list.drawHeaderCallback = rect => EditorGUI.LabelField(rect, header);

        list.drawElementCallback = (rect, index, isActive, isFocused) =>
        {
            SerializedProperty e = listProperty.GetArrayElementAtIndex(index);

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, rect.width - 50, EditorGUIUtility.singleLineHeight),
                e, GUIContent.none
            );
        };

        list.onAddCallback = lst =>
        {
            int index = listProperty.arraySize;
            listProperty.arraySize++;
            lst.index = index;
        };

        list.onRemoveCallback = lst => ReorderableList.defaultBehaviours.DoRemoveButton(lst);
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        myTarget.category = (BaseCardClass.Category)EditorGUILayout.EnumPopup(
            "Category: ", myTarget.category
        );

        switch (myTarget.category)
        {
            case BaseCardClass.Category.PURE_LOGOS:
            case BaseCardClass.Category.CORRUPT_LOGOS:
            case BaseCardClass.Category.PURE_PATHOS:
            case BaseCardClass.Category.CORRUPT_PATHOS:
                DrawRegularCardMenu();
                break;

            case BaseCardClass.Category.STRATEGY:
                DrawSpecialCardMenu();
                break;
        }

        serializedObject.ApplyModifiedProperties();
    }

    void DrawRegularCardMenu()
    {
        myTarget.trust   = EditorGUILayout.IntSlider("Trust: ", myTarget.trust, -10, 10);
        myTarget.support = EditorGUILayout.IntSlider("Support: ", myTarget.support, -20, 20);
        myTarget.cost    = EditorGUILayout.FloatField("Cost Time Minutes (ex 2.30): ", myTarget.cost);

        EditorGUILayout.Space();

        myTarget.hasEffect = EditorGUILayout.Toggle("Card has effect?", myTarget.hasEffect);

        if (!myTarget.hasEffect)
        {
            return;
        }

        myTarget.isAffectedByFriendly = EditorGUILayout.Toggle("Is Affected by Friendly?", myTarget.isAffectedByFriendly);
        EditorGUILayout.Space();

        if (myTarget.isAffectedByFriendly)
        {
            friendlyList.DoLayoutList();
        }
        else
        {
            enemyList.DoLayoutList();
        }

        DrawRequirements();

        if (displayEffects)
        {
            DrawEffects();
        }
    }

    void DrawSpecialCardMenu()
    {
        myTarget.specialEffect = (BaseCardClass.SpecialEffects)EditorGUILayout.EnumPopup(
            "Special Effect: ", myTarget.specialEffect
        );

        switch (myTarget.specialEffect)
        {
            case BaseCardClass.SpecialEffects.DRAW_NEW_HAND:
            case BaseCardClass.SpecialEffects.LOCK_ONE_CARD_POSITION_BOTH:
                break;
            
            case BaseCardClass.SpecialEffects.SEARCH_TOP_X_IN_STACK_FOR_Y_CARDS:
                myTarget.cardsTopStackViewCount = EditorGUILayout.IntSlider(
                    "How many cards can be seen: ", myTarget.cardsTopStackViewCount, 0, 10
                );
                myTarget.cardsFromTopStackCount = EditorGUILayout.IntSlider(
                    "How many cards can be taken: ", myTarget.cardsFromTopStackCount, 0, 3
                );
                break;
            
            case BaseCardClass.SpecialEffects.BRING_BACK_X_CARDS_FROM_DISCARDED:
                myTarget.cardsFromDiscardPileCount = EditorGUILayout.IntSlider(
                    "How many to take from discarded: ", myTarget.cardsFromDiscardPileCount, 0, 5
                );
                break;
        }
    }

    void DrawRequirements()
    {
        myTarget.requirements = (BaseCardClass.Requirements)EditorGUILayout.EnumPopup(
            "Requirements: ", myTarget.requirements
        );

        switch (myTarget.requirements)
        {
            case BaseCardClass.Requirements.NONE:
            case BaseCardClass.Requirements.TRUST_SELF_LOWER_THAN_ENEMY:
            case BaseCardClass.Requirements.TRUST_SELF_LARGER_THAN_ENEMY:
                displayEffects = true;
                break;

            case BaseCardClass.Requirements.TRUST_SELF_LARGER_THAN_LIMIT:
                myTarget.trustSelfLargerThanLimit = EditorGUILayout.IntSlider(
                    "Trust limit: ", myTarget.trustSelfLargerThanLimit, 0, 10
                );
                displayEffects = true;
                break;

            case BaseCardClass.Requirements.TRUST_SELF_LOWER_THAN_LIMIT:
                myTarget.trustSelfLowerThanLimit = EditorGUILayout.IntSlider(
                    "Trust limit: ", myTarget.trustSelfLowerThanLimit, 0, 10
                );
                displayEffects = true;
                break;

            case BaseCardClass.Requirements.SUPPORT_SELF_LARGER_THAN_LIMIT:
                myTarget.supportSelfLargerThanLimit = EditorGUILayout.IntSlider(
                    "Support limit: ", myTarget.supportSelfLargerThanLimit, 0, 100
                );
                displayEffects = true;
                break;

            case BaseCardClass.Requirements.SUPPORT_SELF_LOWER_THAN_LIMIT:
                myTarget.supportSelfLowerThanLimit = EditorGUILayout.IntSlider(
                    "Support limit: ", myTarget.supportSelfLowerThanLimit, 0, 100
                );
                displayEffects = true;
                break;
        }
    }

    void DrawEffects()
    {
        myTarget.effects = (BaseCardClass.Effects)EditorGUILayout.EnumPopup(
            "Effect on Requirement: ", myTarget.effects
        );

        switch (myTarget.effects)
        {
            case BaseCardClass.Effects.ADD_SUPPORT_SELF:
                myTarget.supportOnEffect = EditorGUILayout.IntSlider(
                    "Change support by: ", myTarget.supportOnEffect, -20, 20
                );
                break;
            
            case BaseCardClass.Effects.ADD_TRUST_SELF:
            case BaseCardClass.Effects.ADD_TRUST_ENEMY:
            case BaseCardClass.Effects.REMOVE_TRUST_ENEMY:
                myTarget.trustOnEffect = EditorGUILayout.IntSlider(
                    "Change trust by: ", myTarget.trustOnEffect, -20, 20
                );
                break;
            
            case BaseCardClass.Effects.ADD_TRUST_SUPPORT_SELF:
                myTarget.trustOnEffect = EditorGUILayout.IntSlider(
                    "Change trust by: ", myTarget.trustOnEffect, -20, 20
                );
                myTarget.supportOnEffect = EditorGUILayout.IntSlider(
                    "Change support by: ", myTarget.supportOnEffect, -20, 20
                );
                break;

            case BaseCardClass.Effects.ADD_SUPPORT_SELF_ADD_TRUST_ENEMY:
                myTarget.supportOnEffect = EditorGUILayout.IntSlider(
                    "Change support by: ", myTarget.supportOnEffect, -20, 20
                );
                myTarget.trustOnEffect = EditorGUILayout.IntSlider(
                    "Change trust by: ", myTarget.trustOnEffect, -20, 20
                );
                break;
            
            case BaseCardClass.Effects.GAIN_PERCENTAGE_OF_DIFFERENCE:
                myTarget.percentOnEffect = EditorGUILayout.IntSlider(
                    "Gain X percent of difference: ", myTarget.percentOnEffect, 0, 100
                );
                break;
        }
    }
}
