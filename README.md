The Debate Game version 1.0 24/03/2018
Released by #Respect Studios. 

How to launch the game:
----------------------------------------------------------------------------------------------
1. Open the folder in which all the downloaded files are located in.
2. Doubleclick the .exe file called "TheDebateGame".
3. Once the Unity pre-game launcher appears, select your desired resolution. 
4. Click "Play!".
5. Enjoy the game.

The game must be launched in a 16:9 aspect ratio or else it will not work propperly. 
Recommended resolutions include: 1280x720, 1920x1080, 2560x1440, 3840x2160.

If when clicking the .exe file named "TheDebateGame" Windows prompts you with a message
asking if you would want to run the game despite it not having a known publisher, click "Run".
-----------------------------------------------------------------------------------------------


How to control the game:
-------------------------------------------------------------------------------------
Use a mouse or similar input device to control the cursor and two main mouse buttons. 
-------------------------------------------------------------------------------------

Known bugs: 
---------------------------------------------------------------------------------------------
If clicking the cards too fast, they can end up stuck on the cursor. 
If losing on the second level, music will not be played in the following deck selection stage.
----------------------------------------------------------------------------------------------

Contact information:
--------------------------------------------------------
Website: https://thedebategame.se/
Facebook: https://www.facebook.com/HashtagRespectStudio/
Twitter: https://twitter.com/RESPECTSTUDIO1
--------------------------------------------------------
